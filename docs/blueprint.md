FORMAT: 1A
HOST: http://api.testes.plataformabrasil.org.br

# Plataforma Brasil API
Plataforma Brasil API é um serviço para ser usado através dos aplicativos mobile e do website plataformabrasil.org.br

## Allowed HTTPs requests:

  - `POST` - Creates or updates a resource
  - `PUT` - Updates a resource
  - `GET` - Retrieves a resource or list of resources
  - `DELETE` - Delete a resource

## Typical Server Responses

  - `200` OK - The request was successful (some API calls may return 201 instead).
  - `201` Created - The request was successful and a resource was created.
  - `204` No Content - The request was successful but there is no representation to return (that is, the response is empty).
  - `206` Partial content - The request was successful but the server is delivering only a portion of the content, as requested bu the client via range header.
  - `400` Bad Request - The request could not be understood or was missing required parameters.
  - `401` Unauthorized - Authentication failed.
  - `403` Forbidden - User does not have permissions for the requested operation.
  - `404` Not Found - Resource was not found.
  - `405` Method Not Allowed - Requested method is not supported for the specified resource.
  - `422` Unprocessable Entity - The resource may contain validation errors.
  - `503` Service Unavailable - The service is temporary unavailable (e.g. scheduled Platform Maintenance). Try again later.




# Group Users
Users related resources of the **Plataforma Brasil**


## My Profile [/users]

### User Read [GET]
+ Request
    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body

            {
                "id": 8,
                "nome": "Lucas Pirola",
                "photo_url": "https://graph.facebook.com/10152598541131433/picture?type=large",
                "email": "lucaspirola@gmail.com",
                "data_nascimento": "",
                "ocupacao": "",
                "cidade": "Cidade",
                "estado": "AP",
                "cpf": "",
                "descricao": "",
                "genero": "Masculino",
                "facebook_id": "10152598541131433",
                "sector": "edu",
                "ResetCode": "",
                "CreatedAt": "2015-05-27T21:09:47Z",
                "UpdatedAt": "2015-05-27T21:09:47Z",
                "DeletedAt": "0001-01-01T00:00:00Z"
            }

### User Update [PUT]
+ Request
    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body

## User Config Collection [/users/configs]

### List Config [GET]

+ Request
    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body

### Create Config [POST]
+ Request
    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body

## Invite Friend [/users/invite]

### Send Invite [POST]
+ Request
    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body

## Avatar Upload [/users/avatar]

### Make Upload [POST]

+ Response 200 (application/json; charset=UTF-8)
    + Body


# Group Priorities


## Vote [/priorities/vote]

### Send vote [POST]
+ Request
    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body

## Skip [/priorities/skip]

### Send skip [POST]
+ Request
    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body

## Suggestion [/priorities/choices]

### Create choice as suggestion [POST]
+ Request
    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body


## My Results [/priorities/my_results]

### Read my votes ranked [GET]
+ Request
    + Header

            Authorization: Bearer

+ Response 200 (application/json; charset=UTF-8)
    + Body

            {
                "Acabar com a propaganda eleitoral gratuita no rádio e na televisão": 2,
                "Ampliar as responsabilidades e receitas da União, diminuindo os dos Estados e Municípios": 1,
                "Ampliar o uso obrigatório do orçamento participativo": 1,
                "Criar mais plebiscitos e referendos para participação popular direta": 1,
                "Estado laico": 1,
                "Facilitar a proposição de leis de iniciativa popular, permitindo o uso da internet para obter assinaturas": 1,
                "Guia orientativo sobre o histórico detalhado dos candidatos, suas plataformas de governo e ideias, publicado abertamente na internet e em veículos impressos, com formato padronizado com igual forma para todos os candidatos": 1,
                "O partido precisa escolher se quer que sua campanha seja financiada só por recursos públicos ou por recursos privados": 1,
                "Obrigar todos os representantes eleitos a manter site na internet aberto a comentários, denúncias, reclamações e sugestões": 1,
                "Permitir a campanha paga pela internet": 1,
                "Tornar obrigatória a divulgação na internet da agendas dos agentes públicos eleitos": 1
            }

## Everyone Results [/priorities/results]

### Read all votes ranked [GET]
+ Request
    + Header

            Authorization: Bearer

+ Response 200 (application/json; charset=UTF-8)
    + Body

            {
                "choices": {
                    "-type": "array",
                    "choice": [
                        {
                            "active": {
                                "#text": "true",
                                "-type": "boolean"
                            },
                            "created-at": {
                                "#text": "2015-05-04T03:26:37Z",
                                "-type": "datetime"
                            },
                            "data": "Acabar com o voto secreto de deputados e senadores",
                            "id": {
                                "#text": "48",
                                "-type": "integer"
                            },
                            "losses": {
                                "#text": "298",
                                "-type": "integer"
                            },
                            "score": {
                                "#text": "73.9774",
                                "-type": "float"
                            },
                            "user-created": {
                                "#text": "false",
                                "-type": "boolean"
                            },
                            "wins": {
                                "#text": "849",
                                "-type": "integer"
                            }
                        },
                        {
                            "active": {
                                "#text": "true",
                                "-type": "boolean"
                            },
                            "created-at": {
                                "#text": "2015-05-04T03:26:37Z",
                                "-type": "datetime"
                            },
                            "data": "Facilitar a proposição de leis de iniciativa popular, permitindo o uso da internet para obter assinaturas",
                            "id": {
                                "#text": "32",
                                "-type": "integer"
                            },
                            "losses": {
                                "#text": "304",
                                "-type": "integer"
                            },
                            "score": {
                                "#text": "72.937",
                                "-type": "float"
                            },
                            "user-created": {
                                "#text": "false",
                                "-type": "boolean"
                            },
                            "wins": {
                                "#text": "821",
                                "-type": "integer"
                            }
                        },
                        {
                            "active": {
                                "#text": "true",
                                "-type": "boolean"
                            },
                            "created-at": {
                                "#text": "2015-05-04T18:51:00Z",
                                "-type": "datetime"
                            },
                            "data": "Tornar obrigatória a disponibilização na internet de informações relevantes sobre decisões tomadas pelos representantes (exemplo: votos, vetos, etc.)",
                            "id": {
                                "#text": "77",
                                "-type": "integer"
                            },
                            "losses": {
                                "#text": "107",
                                "-type": "integer"
                            },
                            "score": {
                                "#text": "72.7273",
                                "-type": "float"
                            },
                            "user-created": {
                                "#text": "true",
                                "-type": "boolean"
                            },
                            "wins": {
                                "#text": "287",
                                "-type": "integer"
                            }
                        }
                    ]
                }
            }

## Summary [/priorities/summary]

### Read details about participation [GET]
+ Request
    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body

            {
                "votes": "2",
                "skips": "0",
                "suggestions": "0",
                "invites": "0"
            }




# Group Phase


## Published Phase [/phase]

### Read current phase [GET]

When you open whebsite or mobile we must show the rigth phase to that moment, with this endpoint we know the moment. [95579606](https://www.pivotaltracker.com/story/show/95579606)

+ Response 200 (application/json; charset=UTF-8)
    + Body

            {
                "phase": 2,
                "when_finish": "2015-07-22T00:00:00Z"
            }




# Group Themes


## Themes Collection  [/themes]

### List available themes [GET]

Endpoint useful to show themes at homepage ordered by position - [95579382](https://www.pivotaltracker.com/story/show/95579382)

+ Response 200 (application/json; charset=UTF-8)
    + Body

            [{
                "name": "Favourite programming language?",
                "position": "1",
                "categories": [
                    "Swift",
                    "Python",
                    "Objective-C",
                    "Ruby"
                ],
                "active": true
            }]


## Theme Details [/themes/{id}]

### Read Details [GET]

When user open a theme, some summary info about theme must be available - [95581134](https://www.pivotaltracker.com/story/show/95581134)

+ Parameters
    + id (number) - ID of theme

+ Response 200 (application/json; charset=UTF-8)
    + Body

            {
                "name": "Favourite programming language?",
                "position": "1",
                "categories": [
                    "Swift",
                    "Python",
                    "Objective-C",
                    "Ruby"
                ],
                "active": true
            }


# Group Discussion


## Discussions Collection [/discussions{?page,size,theme_id}]

### List Discussion By Theme [GET]

When users choose a theme to discuss we must show related discussions. If some discussion was flagged, it must be hidden. -
[95580834](https://www.pivotaltracker.com/story/show/95580834) - [95581644](https://www.pivotaltracker.com/story/show/95581644) - [95581498](https://www.pivotaltracker.com/story/show/95581498)

+ Paramenters
    + page (number, optional) - set page to enable infinite pagination on results
    + size (number, optional) - set how many results will return
    + theme_id (number, required) - theme used as base to query discussions

+ Response 200 (application/json; charset=UTF-8)
    + Body

            [{
                "title": "Favourite programming language?",
                "description": " lorem sdfaf das mtes fddd oik dddd",
                "user": {
                    "id": 8,
                    "nome": "Lucas Pirola",
                    "photo_url": "https://graph.facebook.com/10152598541131433/picture?type=large",
                    "sector": "edu"
                },
                "likes": 0,
                "dislikes": 2,
                "count_comments": 11,
                "created_at": "2015-04-12 00:00:00"
            }]


### Create Discussion [POST]

When a user want to go deep about a theme, he needs create a discussion - [95581336](https://www.pivotaltracker.com/story/show/95581336)

+ title (string, optional) - subject of his discussion
+ description (string, require) - argue, topics and related notes about his point
+ categories (array[string], required) - at least 1 category must be choose, max 2.
+ theme_id (number, required) - this discussion belongs to witch theme

+ Request (application/json)

            {
                "title": "Favourite programming language?",
                "description": " lorem sdfaf das mtes fddd oik dddd",
                "categories": [
                    "Swift",
                    "Python"
                ],
                "theme_id": 4
            }

    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body

            {
                "title": "Favourite programming language?",
                "description": " lorem sdfaf das mtes fddd oik dddd",
                "categories": [
                    "Swift",
                    "Python"
                ],
                "user": {
                    "id": 8,
                    "nome": "Lucas Pirola",
                    "photo_url": "https://graph.facebook.com/10152598541131433/picture?type=large",
                    "sector": "edu"
                },
                "likes": 0,
                "dislikes": 2,
                "count_comments": 11,
                "created_at": "2015-04-12 00:00:00"
            }


## Discussion Collection [/discussions/{id}]

### Read Details [GET]

When a user want to argue with other and share other point of view, he goes to discussion page and we need this extra infos. - [95581932](https://www.pivotaltracker.com/story/show/95581932)

+ Response 200 (application/json; charset=UTF-8)
    + Body

            {
                "theme": {
                    "id": 44,
                    "name": "la la al ",
                    "position": 1
                },
                "title": "discussion test title",
                "description": " lorem sdfaf das mtes fddd oik dddd",
                "categories": [
                    "Swift",
                    "Python"
                ],
                "user": {
                    "id": 8,
                    "nome": "Lucas Pirola",
                    "photo_url": "https://graph.facebook.com/10152598541131433/picture?type=large",
                    "sector": "edu"
                },
                "likes": 0,
                "dislikes": 2,
                "count_comments": 11,
                "created_at": "2015-04-12 00:00:00"
            }

## Like Discussion [/discussions/{id}/like]

### Save Like [GET]

Enable user like a discussion. - [95581796](https://www.pivotaltracker.com/story/show/95581796)

+ Parameter
    + id (number, required) - id of discussion
+ Request
    + Header

            Authorization: Bearer
+ Response 201

## DisLike Discussion [/discussions/{id}/dislike]

### Save Dislike [GET]

Enable user dislike a discussion. - [95581830](https://www.pivotaltracker.com/story/show/95581830)

+ Parameter
    + id (number, required) - id of discussion
+ Request
    + Header

            Authorization: Bearer
+ Response 201

## Flag Discussion [/discussions/{id}/flag]

### Save Flag [GET]

Enable user, at any moment, if he feels unconfortable about some discussion he can flag it. An email must be sent to requester. [95581604](https://www.pivotaltracker.com/story/show/95581604)

+ Parameter
    + id (number, required) - id of discussion
+ Request
    + Header

            Authorization: Bearer
+ Response 201




# Group Comments


## Comments Collection [/comments{?page,size,discussion_id}]

### List Comments By Discussion [GET]

When a user choose a discussion, we must show past comments about this to empower her opnion. - [95582024](https://www.pivotaltracker.com/story/show/95582024) - [95581644](https://www.pivotaltracker.com/story/show/95581644)

+ page (number, optional) - set page to enable infinite pagination on results
+ size (number, optional) - set how many results will return
+ discussion_id (number, required) - id of discussion to use as based to query comments

+ Response 200 (application/json; charset=UTF-8)
    + Body

            [{
                "discussion_id": 4,
                "description": " lorem sdfaf das mtes fddd oik dddd"
                "user": {
                    "id": 8,
                    "nome": "Lucas Pirola",
                    "photo_url": "https://graph.facebook.com/10152598541131433/picture?type=large",
                    "sector": "edu"
                },
                "likes": 0,
                "dislikes": 0,
                "created_at": "2015-04-12 00:00:00"
            }]


### Create Comment [POST]

A user can add comments to discussions created by someone else and with this possibly argue in a more specific with others about his vision. - [95590960](https://www.pivotaltracker.com/story/show/95590960)

+ discussion_id (number, required) - this discussion belongs to witch theme
+ description (string, require) - argue, topics and related notes about his point

+ Request (application/json)

            {
                "discussion_id": 4,
                "description": " lorem sdfaf das mtes fddd oik dddd"
            }

    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body

            {
                "discussion_id": 4,
                "description": " lorem sdfaf das mtes fddd oik dddd"
                "user": {
                    "id": 8,
                    "nome": "Lucas Pirola",
                    "photo_url": "https://graph.facebook.com/10152598541131433/picture?type=large",
                    "sector": "edu"
                },
                "likes": 0,
                "dislikes": 0,
                "created_at": "2015-04-12 00:00:00"
            }

## Like Comment [/comments/{id}/like]

### Save Like [GET]

Enable users like comments - [95581796](https://www.pivotaltracker.com/story/show/95581796)

+ Request
    + Header

            Authorization: Bearer
+ Response 201

## DisLike Comment [/comments/{id}/dislike]

### Save Dislike [GET]

Enable user dislike any comment. [95581830](https://www.pivotaltracker.com/story/show/95581830)

+ Request
    + Header

            Authorization: Bearer
+ Response 201

## Flag Comment [/comments/{id}/flag]

### Save Flag [GET]

Enable user, at any moment, if he feels unconfortable about some comment, he can flag it. An email must be sent to requester. [95581604](https://www.pivotaltracker.com/story/show/95581604)

+ Request
    + Header

            Authorization: Bearer
+ Response 201

# Group Handy Man

## Handy Man Collection [/handy-man{?categories}]

### List Handy Man content by categories [GET]

When users want some help with some subject, based on categories we will show the contents - [95582122](https://www.pivotaltracker.com/story/show/95582122)

+ Parameter
    + categories (string, optional) -

+ Request
    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body

            [{
                "id": 1,
                "title": "Favourite programming language?",
                "caption": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.",
                "image": "Swift",
                "url": "Swift",
                "category": ["genero", "partido"]
                "active": true,
                "created_at": "2015-04-12 00:00:00"
                "created_at": "2015-04-12 00:00:00",
                "updated_at": "2015-04-12 00:00:00"
            }]

# Group Admin


## Users Collection [/admin/users]

### Users List [GET]
+ Request
    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body

## Choices Collection [/admin/choices]

### Choices List [GET]
+ Request
    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body

### Choice Create [POST]
+ Request
    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body


## Choice Collection [/admin/choices/{id}]

### Choice Update [PUT]
+ Parameters
    + id (number) - ID of choice
+ Request
    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body

### Choice Read [GET]
+ Parameters
    + id (number) - ID of choice
+ Request
    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body


## Theme Collection [/admin/themes]

### Theme List[GET]

List all themes [95581156](https://www.pivotaltracker.com/story/show/95581156)

+ Response 200 (application/json; charset=UTF-8)
    + Body

            [{
                "name": "Favourite programming language?",
                "position": "1",
                "categories": [
                    "Swift",
                    "Python",
                    "Objective-C",
                    "Ruby"
                ],
                "active": true
            }]

### Theme Create[POST]

Theme is basic structure to system. Discussions and Comments are filtered by it. - [95579768](https://www.pivotaltracker.com/story/show/95579768)

+ name (string) - The theme title
+ position (number) - Position witch he must appear on website
+ categories (array[string]) - A collection of categories available to user assign when creating discussion
+ active (boolean) - If true, he must be available to website

+ Request (application/json)

            {
                "name": "Favourite programming language?",
                "position": "1",
                "categories": [
                    "Swift",
                    "Python",
                    "Objective-C",
                    "Ruby"
                ],
                "active": true
            }
    + Header

            Authorization: Bearer

+ Response 200 (application/json; charset=UTF-8)
    + Body

            {
                "name": "Favourite programming language?",
                "position": "1",
                "categories": [
                    "Swift",
                    "Python",
                    "Objective-C",
                    "Ruby"
                ],
                "active": true,
                "created_at": "2015-04-12 00:00:00",
                "updated_at": "2015-04-12 00:00:00"
            }


## Phase collection [/admin/phase]

### Update published phase [PUT]

The cycle is split in 3 phases, here you can set wich is active or published.  [95579342](https://www.pivotaltracker.com/story/show/95579342)

+ phase_num (number, 2) - Active phase
+ when_finish (string, 2015-07-22) - limit date to show on website to users

+ Request (application/json)

            {
                "phase_num": 1,
                "when_finish": "2015-07-22T00:00:00Z"
            }
    + Header

            Authorization: Bearer

+ Response 200 (application/json; charset=UTF-8)
    + Body

            {
                "phase": 2,
                "when_finish": "2015-07-22T00:00:00Z"
            }

## Handy Man Collection [/admin/handy-man]

### List Handy Man [GET]

All contents available to use - [95582122](https://www.pivotaltracker.com/story/show/95582122)

+ Request
    + Header

            Authorization: Bearer
+ Response 200 (application/json; charset=UTF-8)
    + Body

            [{
                "id": 1,
                "title": "Favourite programming language?",
                "caption": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.",
                "image": "Swift",
                "url": "Swift",
                "category": ["genero", "partido"]
                "active": true,
                "created_at": "2015-04-12 00:00:00"
                "created_at": "2015-04-12 00:00:00",
                "updated_at": "2015-04-12 00:00:00"
            }]

### Create Handy Man content [POST]

Curated content to help users with more information about discussions - [95582064](https://www.pivotaltracker.com/story/show/95582064)

+ title (string) - The title about content
+ caption (text) - The caption to help user understand content
+ image (string) - Image to represent
+ url (string) - URL to external content, must be url
+ category (array[number]) - Category related
+ active (boolean) - If true, show at website

+ Request (application/json)

            {
                "id": 1,
                "title": "Favourite programming language?",
                "caption": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.",
                "image": "Swift",
                "url": "Swift",
                "category": ["genero", "partido"],
                "active": true,
                "created_at": "2015-04-12 00:00:00",
                "updated_at": "2015-04-12 00:00:00"
            }
    + Header

            Authorization: Bearer

+ Response 200 (application/json; charset=UTF-8)
    + Body

            {
                "id": 1,
                "title": "Favourite programming language?",
                "caption": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.",
                "image": "Swift",
                "url": "Swift",
                "category": ["genero", "partido"],
                "active": true,
                "created_at": "2015-04-12 00:00:00",
                "updated_at": "2015-04-12 00:00:00"
            }
