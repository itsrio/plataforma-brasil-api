package serializers

import (
	"time"

	"github.com/jinzhu/gorm"
	"github.com/nucleo-digital/plataforma-brasil-api/models"
)

type CategorySerializer struct {
	Name string `json:"name"`
}

type UserSerializer struct {
	ID       int    `json:"id"`
	Nome     string `json:"nome"`
	PhotoUrl string `json:"photo_url"`
	Sector   string `json:"sector"`
}

type ThemeSerializer struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	Position int    `json:"position"`
}

type DiscussionSerializer struct {
	ID              int                  `json:"id"`
	Theme           ThemeSerializer      `json:"theme"`
	Title           string               `json:"title"`
	Description     string               `json:"description"`
	Categories      []CategorySerializer `json:"categories"`
	User            UserSerializer       `json:"user"`
	Score           int                  `json:"score"`
	MyLikes         int                  `json:"my_likes"`
	Likes           int                  `json:"likes"`
	DisLikes        int                  `json:"dislikes"`
	MyDisLikes      int                  `json:"my_dislikes"`
	TotalComplaints int                  `json:"total_complaints"`
	CountComments   int                  `json:"count_comments"`
	CreatedAt       time.Time            `json:"created_at"`
}

type DiscussionTopicNotificationSerializer struct {
	ID                          int                         `json:"id"`
	Opened                      bool                        `json:"opened"`
	DiscussionSerializer        DiscussionSerializer        `json:"discussion"`
	TemplateName                string                      `json:"template_name"`
	FromUserId                  int                         `json:"from_user_id"`
	FromUserName                string                      `json:"from_user_name"`
	FromUserPhotoUrl            string                      `json:"from_user_photo_url"`
	FromUserSector              string                      `json:"from_user_sector"`
	DiscussionCommentSerializer DiscussionCommentSerializer `json:"discussion_comment_resource"`
	CreatedAt                   time.Time                   `json:"created_at"`
}

type DiscussionCommentTopicNotificationsSerializer struct {
	ID                          int                         `json:"id"`
	Opened                      bool                        `json:"opened"`
	DiscussionSerializer        DiscussionSerializer        `json:"discussion"`
	TemplateName                string                      `json:"template_name"`
	FromUserId                  int                         `json:"from_user_id"`
	FromUserName                string                      `json:"from_user_name"`
	FromUserPhotoUrl            string                      `json:"from_user_photo_url"`
	FromUserSector              string                      `json:"from_user_sector"`
	DiscussionCommentSerializer DiscussionCommentSerializer `json:"discussion_comment_resource"`
	CreatedAt                   time.Time                   `json:"created_at"`
}

type DiscussionCommentSerializer struct {
	ID              int            `json:"id"`
	DiscussionID    int            `json:"discussion_id"`
	Description     string         `json:"description"`
	User            UserSerializer `json:"user"`
	MyLikes         int            `json:"my_likes"`
	Likes           int            `json:"likes"`
	DisLikes        int            `json:"dislikes"`
	MyDisLikes      int            `json:"my_dislikes"`
	TotalComplaints int            `json:"total_complaints"`
	Visible         bool           `json:"visible"`
	CreatedAt       time.Time      `json:"created_at"`
}

// BuildFromResource build a serialized discussion object
func (ds *DiscussionSerializer) BuildFromResource(dis *models.Discussion, db *gorm.DB, userID int) {
	db.Model(models.Category{}).
		Select("categories.*").
		Joins("inner join category_discussions cd on cd.category_id = categories.id").
		Where("cd.discussion_id = ?", dis.ID).
		Scan(&ds.Categories)

	db.Model(models.Theme{}).
		Select("themes.*").
		Joins("inner join discussions d on d.theme_id = themes.id").
		Where("d.id = ?", dis.ID).
		Scan(&ds.Theme)

	db.Model(models.User{}).
		Select("users.*").
		Joins("inner join discussions d on d.user_id = users.id").
		Where("d.id = ?", dis.ID).
		Scan(&ds.User)

	var scoresPluck []int
	db.Model(models.DiscussionLike{}).Where("discussion_id = ?", dis.ID).Count(&ds.Likes)
	db.Model(models.DiscussionDislike{}).Where("discussion_id = ?", dis.ID).Count(&ds.DisLikes)
	db.Model(models.DiscussionComment{}).Where("discussion_id = ?", dis.ID).Count(&ds.CountComments)
	db.Model(models.DiscussionScore{}).Select("score").Where("discussion_id = ?", dis.ID).Pluck("score", &scoresPluck)
	db.Model(models.DiscussionComplaint{}).Where("discussion_id = ?", dis.ID).Count(&ds.TotalComplaints)

	if len(scoresPluck) > 0 {
		ds.Score = scoresPluck[0]
	}

	if userID > 0 {
		db.Model(models.DiscussionLike{}).
			Where("discussion_id = ? and user_id = ?", dis.ID, userID).Count(&ds.MyLikes)

		db.Model(models.DiscussionDislike{}).
			Where("discussion_id = ? and user_id = ?", dis.ID, userID).Count(&ds.MyDisLikes)
	}

	ds.ID = dis.ID
	ds.Title = dis.Title
	ds.CreatedAt = dis.CreatedAt
	ds.Description = dis.Description
}

// BuildFromResource build a serialized discussion comment object
func (ds *DiscussionCommentSerializer) BuildFromResource(dis *models.DiscussionComment, db *gorm.DB, userID int) {
	db.Model(models.User{}).
		Select("users.*").
		Joins("inner join discussion_comments d on d.user_id = users.id").
		Where("d.id = ?", dis.ID).
		Scan(&ds.User)

	db.Model(models.DiscussionCommentLike{}).Where("discussion_comment_id = ?", dis.ID).Count(&ds.Likes)
	db.Model(models.DiscussionCommentDisLike{}).Where("discussion_comment_id = ?", dis.ID).Count(&ds.DisLikes)
	db.Model(models.DiscussionCommentComplaint{}).Where("discussion_comment_id = ?", dis.ID).Count(&ds.TotalComplaints)

	if userID > 0 {
		db.Model(models.DiscussionCommentLike{}).
			Where("discussion_comment_id = ? and user_id = ?", dis.ID, userID).Count(&ds.MyLikes)

		db.Model(models.DiscussionCommentDisLike{}).
			Where("discussion_comment_id = ? and user_id = ?", dis.ID, userID).Count(&ds.MyDisLikes)
	}

	ds.ID = dis.ID
	ds.DiscussionID = dis.DiscussionID
	ds.Description = dis.Description
	ds.Visible = dis.Visible
	ds.CreatedAt = dis.CreatedAt
}

// BuildFromResource build a serialized object for discussion topic notification
func (dtns *DiscussionTopicNotificationSerializer) BuildFromResource(dtn *models.DiscussionTopicNotification, db *gorm.DB, userID int) {
	var discussionSerializer DiscussionSerializer

	discussion := dtn.FindDiscussion(db)
	discussionSerializer.BuildFromResource(&discussion, db, userID)

	dtns.DiscussionSerializer = discussionSerializer
	dtns.TemplateName = dtn.TemplateName
	dtns.CreatedAt = dtn.CreatedAt
	dtns.ID = dtn.ID
	dtns.Opened = dtn.Opened

	if dtn.DiscussionLikeID != 0 {
		var discussionLike models.DiscussionLike

		db.Model(&dtn).Related(&discussionLike)
		dtns.FillFromUser(discussionLike.UserID, db)
	} else if dtn.DiscussionDislikeID != 0 {
		var discussionDislike models.DiscussionDislike

		db.Model(&dtn).Related(&discussionDislike)
		dtns.FillFromUser(discussionDislike.UserID, db)
	} else if dtn.DiscussionCommentTopicID != 0 {
		var discussionCommentTopic models.DiscussionCommentTopic
		var discussionComment models.DiscussionComment
		var discussionCommentSerializer DiscussionCommentSerializer

		db.Model(&dtn).Related(&discussionCommentTopic)
		db.Model(&discussionCommentTopic).Related(&discussionComment)
		discussionCommentSerializer.BuildFromResource(&discussionComment, db, userID)

		dtns.FillFromUser(discussionComment.UserID, db)
		dtns.DiscussionCommentSerializer = discussionCommentSerializer
	}
}

//BuildFromResource discussion comment topic notifications
func (dtns *DiscussionCommentTopicNotificationsSerializer) BuildFromResource(dctn *models.DiscussionCommentTopicNotification, db *gorm.DB, userID int) {
	var discussionCommentSerializer DiscussionCommentSerializer
	var discussionComment models.DiscussionComment
	var discussionCommentTopic models.DiscussionCommentTopic

	db.Model(&dctn).Related(&discussionCommentTopic)
	db.Model(&discussionCommentTopic).Related(&discussionComment)

	discussionCommentSerializer.BuildFromResource(&discussionComment, db, userID)

	dtns.DiscussionCommentSerializer = discussionCommentSerializer
	dtns.TemplateName = dctn.TemplateName
	dtns.CreatedAt = dctn.CreatedAt
	dtns.ID = dctn.ID
	dtns.Opened = dctn.Opened

	if dctn.DiscussionCommentLikeID != 0 {
		var discussionCommentLike models.DiscussionCommentLike

		db.Model(&dctn).Related(&discussionCommentLike)
		dtns.FillFromUser(discussionCommentLike.UserID, db)
	} else if dctn.DiscussionCommentDisLikeID != 0 {
		var discussionCommentDisLike models.DiscussionCommentDisLike

		db.Model(&dctn).Related(&discussionCommentDisLike)
		dtns.FillFromUser(discussionCommentDisLike.UserID, db)
	}
}

// FillFromUser helper method to fill user info from a given user id
func (dtns *DiscussionTopicNotificationSerializer) FillFromUser(userID int, db *gorm.DB) {
	var fromUser models.User
	db.Find(&fromUser, "id = ?", userID)
	dtns.FromUserId = fromUser.ID
	dtns.FromUserName = fromUser.Nome
	dtns.FromUserPhotoUrl = fromUser.PhotoUrl
	dtns.FromUserSector = fromUser.Sector
}

// FillFromUser helper method to fill user info from a given user id
func (dtns *DiscussionCommentTopicNotificationsSerializer) FillFromUser(userID int, db *gorm.DB) {
	var fromUser models.User
	db.Find(&fromUser, "id = ?", userID)
	dtns.FromUserId = fromUser.ID
	dtns.FromUserName = fromUser.Nome
	dtns.FromUserPhotoUrl = fromUser.PhotoUrl
	dtns.FromUserSector = fromUser.Sector
}
