package middlewares

import (
	"github.com/codegangsta/negroni"
	"github.com/gorilla/context"

	"github.com/joho/godotenv"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"

	"log"
	"net/http"
	"os"
)

func GetDb(r *http.Request) *gorm.DB {
	if rv := context.Get(r, "db"); rv != nil {
		return rv.(*gorm.DB)
	}

	return nil
}

func NewDbConnection() negroni.HandlerFunc {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	var db_url string
	if db_url = os.Getenv("DATABASE_URL"); db_url == "" {
		db_url = "root@/platbr?charset=utf8&parseTime=True"
	}
	db, err := gorm.Open("mysql", db_url)
	db.LogMode(true)

	if err != nil {
		log.Fatal(err)
	}

	return negroni.HandlerFunc(func(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		context.Set(r, "db", db.New())
		next(rw, r)
		context.Clear(r)
	})
}
