package controllers

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"github.com/mholt/binding"
	"github.com/nucleo-digital/plataforma-brasil-api/middlewares"
	"github.com/nucleo-digital/plataforma-brasil-api/models"
	"github.com/unrolled/render"
)

// NORMAL HANDLERS
// ShowThemeHandler shows requested theme
func ShowThemeHandler(w http.ResponseWriter, req *http.Request) {
	var theme models.Theme

	r := render.New()
	db := middlewares.GetDb(req)
	vars := mux.Vars(req)
	id := vars["id"]

	query := db.Where("active = ? AND id = ?", true, id).Find(&theme)
	if query.Error != nil {
		r.JSON(w, http.StatusNotFound, map[string]string{"error": "Não foi possivel encontrar o tema"})
	} else {
		r.JSON(w, http.StatusOK, getThemeCategories(&theme, db))
	}
}

// IndexThemesHandler handles with active and ordered theme list
func IndexThemesHandler(w http.ResponseWriter, req *http.Request) {
	var themes []models.Theme

	r := render.New()
	db := middlewares.GetDb(req)

	db.Where("active = ?", true).Order("position asc").Find(&themes)
	r.JSON(w, http.StatusOK, getThemesCategories(themes, db))
}

// ADMIN HANDLERS
// AdminIndexThemesHandler handles with theme list for admin
func AdminIndexThemesHandler(w http.ResponseWriter, req *http.Request) {
	var themes []models.Theme

	r := render.New()
	db := middlewares.GetDb(req)

	db.Find(&themes)
	r.JSON(w, http.StatusOK, getThemesCategories(themes, db))
}

// CreateThemeHandler handlers with creation of new theme
func CreateThemeHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)
	themeForm := &models.ThemeForm{}
	if binding.Bind(req, themeForm).Handle(w) {
		return
	}

	theme := &models.Theme{}
	theme.Name = themeForm.Theme.Name
	theme.Categories = themeForm.Theme.Categories
	theme.Position = themeForm.Theme.Position
	theme.Active = themeForm.Theme.Active
	theme.CreatedAt = themeForm.Theme.CreatedAt
	theme.Categories = asyncRemapCategories(theme.Categories, db)
	saveQuery := db.Save(&theme)

	if saveQuery.Error != nil {
		r.JSON(w, http.StatusBadRequest, map[string]string{"error": "ocorreu um erro ao tentar salvar o tema"})
	} else {
		r.JSON(w, http.StatusOK, getThemeCategories(theme, db))
	}
}

// getThemesCategories load categories from a list of themes
func getThemesCategories(themes []models.Theme, db *gorm.DB) []models.Theme {
	for i, _ := range themes {
		getThemeCategories(&themes[i], db)
	}

	return themes
}

// getThemeCategories load categories for an single theme
func getThemeCategories(t *models.Theme, db *gorm.DB) *models.Theme {
	var categories []models.Category

	db.Model(&t).Association("Categories").Find(&categories)
	t.Categories = categories

	return t
}
