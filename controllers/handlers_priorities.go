package controllers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/clbanning/mxj"
	"github.com/freehaha/token-auth"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"github.com/mholt/binding"
	"github.com/nucleo-digital/plataforma-brasil-api/middlewares"
	"github.com/nucleo-digital/plataforma-brasil-api/models"
	"github.com/unrolled/render"
)

const API_HOST = "http://54.232.255.49/"
const API_USERNAME = "lucaspirola@gmail.com"
const API_PASSWORD = "123"

func MigrationHandler(w http.ResponseWriter, req *http.Request) {
	db := middlewares.GetDb(req)
	db.AutoMigrate(&models.User{})
	db.AutoMigrate(&models.Vote{})
	db.AutoMigrate(&models.Skip{})
	db.AutoMigrate(&models.Suggestion{})
	db.AutoMigrate(&models.Configuration{})
	db.AutoMigrate(&models.SiteConfiguration{})
	db.AutoMigrate(&models.Invite{})
	db.AutoMigrate(&models.Phase{})
	db.AutoMigrate(&models.Category{})
	db.AutoMigrate(&models.Theme{})
	db.AutoMigrate(&models.Discussion{})
	db.AutoMigrate(&models.DiscussionLike{})
	db.AutoMigrate(&models.DiscussionDislike{})
	db.AutoMigrate(&models.DiscussionComment{})
	db.AutoMigrate(&models.DiscussionCommentLike{})
	db.AutoMigrate(&models.DiscussionCommentDisLike{})
	db.AutoMigrate(&models.DiscussionComplaint{})
	db.AutoMigrate(&models.DiscussionCommentComplaint{})
	db.AutoMigrate(&models.DiscussionTopic{})
	db.AutoMigrate(&models.DiscussionCommentTopic{})
	db.AutoMigrate(&models.DiscussionTopicNotification{})
	db.AutoMigrate(&models.DiscussionCommentTopicNotification{})

	db.Table("discussions").Where("visible is null").Update("visible", 1)
	db.Table("discussion_comments").Where("visible is null").Update("visible", 1)

	BuildTopicsForCreatedDiscussions(db)
	BuildTopicsForCreatedDiscussionComments(db)

	//maybe in future ;)
	//db.Exec("ALTER TABLE users ADD FULLTEXT (nome)")
}

// BuildTopicsForCreatedDiscussions build topis for already created discussions
func BuildTopicsForCreatedDiscussions(db *gorm.DB) {
	var discussions []models.Discussion

	db.Model(&models.Discussion{}).
		Joins(`
			left outer join discussion_topics dt
			on dt.discussion_id = discussions.id
		`).
		Where("dt.id is null").
		Find(&discussions)

	for _, item := range discussions {
		topic := models.DiscussionTopic{DiscussionID: item.ID}
		db.Create(&topic)
	}
}

// BuildTopicsForCreatedDiscussionComments build topis for already created discussions comments
func BuildTopicsForCreatedDiscussionComments(db *gorm.DB) {
	var discussionComments []models.DiscussionComment

	db.Model(&models.DiscussionComment{}).
		Joins(`
			left outer join discussion_comment_topics dt
			on dt.discussion_comment_id = discussion_comments.id
		`).
		Where("dt.id is null").
		Find(&discussionComments)

	for _, item := range discussionComments {
		topic := models.DiscussionCommentTopic{DiscussionCommentID: item.ID}
		db.Create(&topic)
	}
}

func PairwiseGetJSON(path string, params url.Values) []byte {

	client := &http.Client{}
	req, err := http.NewRequest("GET", API_HOST+path+params.Encode(), nil)
	req.SetBasicAuth(API_USERNAME, API_PASSWORD)
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	bodyText, err := ioutil.ReadAll(resp.Body)

	return bodyText
}

func PairwiseGet(path string, params url.Values) mxj.Map {

	client := &http.Client{}

	req, err := http.NewRequest("GET", API_HOST+path+params.Encode(), nil)
	req.SetBasicAuth(API_USERNAME, API_PASSWORD)
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	// log.Printf("%v", resp)
	bodyText, err := ioutil.ReadAll(resp.Body)
	s := string(bodyText)
	m, err := mxj.NewMapXml([]byte(s))

	return m
}

func PairwisePut(path string, params url.Values) mxj.Map {
	client := &http.Client{}
	req, err := http.NewRequest("PUT", API_HOST+path+params.Encode(), nil)
	if err != nil {
		log.Printf("%v", err)
	}
	req.SetBasicAuth(API_USERNAME, API_PASSWORD)
	resp, err := client.Do(req)

	bodyText, err := ioutil.ReadAll(resp.Body)
	s := string(bodyText)
	// log.Printf("%v", req)
	m, err := mxj.NewMapXml([]byte(s))

	return m
}

func PairwisePost(path string, params url.Values) mxj.Map {
	client := &http.Client{}
	req, err := http.NewRequest("POST", API_HOST+path+params.Encode(), nil)
	if err != nil {
		log.Printf("%v", err)
	}
	req.SetBasicAuth(API_USERNAME, API_PASSWORD)
	resp, err := client.Do(req)

	bodyText, err := ioutil.ReadAll(resp.Body)
	s := string(bodyText)
	// log.Printf("%v", req)
	m, err := mxj.NewMapXml([]byte(s))

	return m
}

func XmlGetValue(m mxj.Map, path string) string {

	tmp, err := m.ValuesForPath(path)
	if err != nil {
		log.Printf("%v", err)
	}

	if len(tmp) == 0 {
		return ""
	}

	return fmt.Sprintf("%v", tmp[0])
}

// ListChoicesHandler list of all choices avaible via pairwise
func ListChoicesHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	v := url.Values{}
	// v.Set("include_inactive", "1")
	v.Set("limit", "1000")
	v.Set("orderby", req.FormValue("orderBy"))
	v.Set("orderdirection", req.FormValue("orderDirection"))
	b := PairwiseGetJSON("questions/1/choices.json?", v)

	var f AdminChoicesAOI
	err := json.Unmarshal(b, &f)
	if err != nil {
		log.Println("Error decodign choices ", err)
	}

	var g AdminChoicesList
	cc := []Choice{}
	for _, elem := range f {
		cc = append(cc, elem.Choice)
	}
	g.Choices = cc
	r.JSON(w, http.StatusOK, g)
}

func ChoicesHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()

	params := &models.QueryStringQuestionShow{
		req.FormValue("visitor_identifier"),
		req.FormValue("with_prompt"),
		req.FormValue("with_appearance"),
		req.FormValue("with_visitor_stats"),
		req.FormValue("algorithm"),
		req.FormValue("version"),
	}

	v := url.Values{}
	v.Set("visitor_identifier", params.VisitorIdentifier)
	v.Set("with_prompt", params.WithPrompt)
	v.Set("with_appearance", params.WithAppearance)
	v.Set("with_visitor_stats", params.WithVisitorStats)
	v.Set("algorithm", params.Algorithm)
	v.Set("version", params.Version)

	m := PairwiseGet("questions/1.xml?", v)
	mm := PairwiseGet(fmt.Sprintf("questions/1/prompts/%v.xml?", XmlGetValue(m, "question.picked_prompt_id")), v)

	ms := &models.PromptShow{
		XmlGetValue(m, "question.name"),
		XmlGetValue(mm, "prompt.created-at.#text"),
		XmlGetValue(mm, "prompt.id.#text"),
		XmlGetValue(mm, "prompt.left-choice-id.#text"),
		XmlGetValue(mm, "prompt.question-id.#text"),
		XmlGetValue(mm, "prompt.right-choice-id.#text"),
		XmlGetValue(mm, "prompt.updated-at.#text"),
		XmlGetValue(mm, "prompt.votes-count.#text"),
		XmlGetValue(mm, "prompt.left-choice-text"),
		XmlGetValue(mm, "prompt.right-choice-text"),
	}

	r.JSON(w, http.StatusOK, ms)
}

func NewChoiceHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)

	ChoiceForm := new(models.ParamsChoiceCreate)
	errs := binding.Bind(req, ChoiceForm)
	if errs.Handle(w) {
		return
	}

	v := url.Values{}
	v.Set("choice[visitor_identifier]", ChoiceForm.VisitorIdentifier)
	v.Add("choice[data]", template.HTMLEscapeString(ChoiceForm.Data))
	v.Add("choice[local_identifier]", ChoiceForm.LocalIdentifier)

	m := PairwisePost("questions/1/choices.xml?", v)

	ms := &models.ChoiceCreate{
		XmlGetValue(m, "choice.active.#text"),
		XmlGetValue(m, "choice.created-at.#text"),
		XmlGetValue(m, "choice.creator-id.#text"),
		XmlGetValue(m, "choice.data.#text"),
		XmlGetValue(m, "choice.id.#text"),
		XmlGetValue(m, "choice.item-id.#text"),
		XmlGetValue(m, "choice.local-identifier.#text"),
		XmlGetValue(m, "choice.loss-count.#text"),
		XmlGetValue(m, "choice.losses.#text"),
		XmlGetValue(m, "choice.position.#text"),
		XmlGetValue(m, "choice.prompt-id.#text"),
		XmlGetValue(m, "choice.prompts-count.#text"),
		XmlGetValue(m, "choice.prompts-on-the-left-count.#text"),
		XmlGetValue(m, "choice.prompts-on-the-right-count.#text"),
		XmlGetValue(m, "choice.question-id.#text"),
		XmlGetValue(m, "choice.ratings.#text"),
		XmlGetValue(m, "choice.request-id.#text"),
		XmlGetValue(m, "choice.score.#text"),
		XmlGetValue(m, "choice.tracking.#text"),
		XmlGetValue(m, "choice.updated-at.#text"),
		XmlGetValue(m, "choice.votes-count.#text"),
		XmlGetValue(m, "choice.wins.#text"),
	}

	t := tauth.Get(req)
	UserID := t.Claims("id").(float64)
	sg := models.Suggestion{UserID: int(UserID), ChoiceText: ChoiceForm.Data}

	db.Create(&sg)
	r.JSON(w, http.StatusOK, ms)
}

func VoteHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)

	VoteForm := new(models.ParamsPromptVote)
	errs := binding.Bind(req, VoteForm)
	if errs.Handle(w) {
		return
	}

	v := url.Values{}
	v.Set("vote[visitor_identifier]", VoteForm.Vote.VisitorIdentifier)
	v.Add("vote[direction]", VoteForm.Vote.Direction)
	v.Add("vote[question_id]", "1")
	// v.Add("vote[appearance_lookup]", VoteForm.Vote.AppearanceLookup)
	// v.Add("vote[time_viewed]", VoteForm.Vote.TimeViewed)
	// v.Add("vote[force_invalid_vote]", VoteForm.Vote.ForceInvalidVote)
	// v.Add("vote[tracking]", VoteForm.Vote.Tracking)
	v.Add("next_prompt[visitor_identifier]", VoteForm.NextPrompt.VisitorIdentifier)
	// v.Add("next_prompt[with_appearance]", VoteForm.NextPrompt.WithAppearance)
	// v.Add("next_prompt[algorithm]", VoteForm.NextPrompt.Algorithm)
	// v.Add("next_prompt[with_visitor_stats]", VoteForm.NextPrompt.WithVisitorStats)

	prompt_id := req.FormValue("prompt_id")
	m := PairwisePost("questions/1/prompts/"+prompt_id+"/vote.xml?", v)

	ms := &models.PromptVoteOrSkip{
		XmlGetValue(m, "prompt.created-at.#text"),
		XmlGetValue(m, "prompt.id.#text"),
		XmlGetValue(m, "prompt.question-id.#text"),
		XmlGetValue(m, "prompt.updated-at.#text"),
		XmlGetValue(m, "prompt.votes-count.#text"),
		XmlGetValue(m, "prompt.left-choice-text"),
		XmlGetValue(m, "prompt.right-choice-text"),
		XmlGetValue(m, "prompt.left-choice-id.#text"),
		XmlGetValue(m, "prompt.right-choice-id.#text"),
	}

	t := tauth.Get(req)
	UserID := t.Claims("id").(float64)
	cid, _ := strconv.Atoi(VoteForm.Vote.ChoiceID)
	vt := models.Vote{UserID: int(UserID), ChoiceText: VoteForm.Vote.ChoiceText, ChoiceID: cid}

	db.Create(&vt)

	r.JSON(w, http.StatusOK, ms)
}

func SkipHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)

	// parse json
	VoteForm := new(models.ParamsPromptVote)
	errs := binding.Bind(req, VoteForm)
	if errs.Handle(w) {
		return
	}

	// build url with params
	v := url.Values{}
	v.Set("vote[visitor_identifier]", VoteForm.Vote.VisitorIdentifier)
	v.Add("vote[direction]", VoteForm.Vote.Direction)
	v.Add("vote[question_id]", "1")
	v.Add("next_prompt[visitor_identifier]", VoteForm.NextPrompt.VisitorIdentifier)

	prompt_id, _ := strconv.Atoi(req.FormValue("prompt_id"))
	m := PairwisePost("questions/1/prompts/"+req.FormValue("prompt_id")+"/skip.xml?", v)

	// extract values from xml to struct
	ms := &models.PromptVoteOrSkip{
		XmlGetValue(m, "prompt.created-at.#text"),
		XmlGetValue(m, "prompt.id.#text"),
		XmlGetValue(m, "prompt.question-id.#text"),
		XmlGetValue(m, "prompt.updated-at.#text"),
		XmlGetValue(m, "prompt.votes-count.#text"),
		XmlGetValue(m, "prompt.left-choice-text"),
		XmlGetValue(m, "prompt.right-choice-text"),
		XmlGetValue(m, "prompt.left-choice-id.#text"),
		XmlGetValue(m, "prompt.right-choice-id.#text"),
	}

	t := tauth.Get(req)
	UserID := t.Claims("id").(float64)
	sk := models.Skip{UserID: int(UserID), PromptID: prompt_id}

	db.Create(&sk)

	r.JSON(w, http.StatusOK, ms)
}

func GetAllNodesXml(m mxj.Map) mxj.Map {
	leafnodes := m.LeafNodes()
	log.Printf("%v", leafnodes)
	return nil
}

func GetAllValuesXml(m mxj.Map) mxj.Map {
	leafvalues := m.LeafValues()
	log.Printf("%v", leafvalues)
	return nil
}

func AllResultsHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	v := url.Values{}
	m := PairwiseGet("questions/1/choices.xml?limit=6", v)

	jsonVal, jerr := m.Json()
	if jerr != nil {
		// handle error
	}

	mapVal, merr := mxj.NewMapJson(jsonVal)
	if merr != nil {
		// handle error
	}

	// TODO: map json to struct
	// var data Choices
	// err := json.Unmarshal(jsonVal, &data)
	// if err != nil {
	//     // handle error
	// }

	r.JSON(w, http.StatusOK, mapVal)
}

func MyResultsHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()

	t := tauth.Get(req)
	visitor_identifier := t.Claims("email")
	visitor_identifier = strings.Replace(fmt.Sprintf("%v", visitor_identifier), "@", "", 5)
	visitor_identifier = strings.Replace(fmt.Sprintf("%v", visitor_identifier), ".", "", 5)

	v := url.Values{}
	v.Set("question_id", "1")
	bodyText := PairwiseGetJSON("visitors/"+fmt.Sprintf("%v", visitor_identifier)+"/votes.json?", v)
	var data models.MyResults
	json.Unmarshal(bodyText, &data)

	m := make(map[string]int)

	for i := range data.Votes {
		if data.Votes[i].Winner == "left" {
			elem, ok := m[data.Votes[i].LeftChoiceData]
			if !ok {
				m[data.Votes[i].LeftChoiceData] = 1
			} else {
				m[data.Votes[i].LeftChoiceData] = elem + 1
			}
		} else {
			elem, ok := m[data.Votes[i].RightChoiceData]
			if !ok {
				m[data.Votes[i].RightChoiceData] = 1
			} else {
				m[data.Votes[i].RightChoiceData] = elem + 1
			}
		}
	}

	r.JSON(w, http.StatusOK, m)
}

func MySummaryHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)

	t := tauth.Get(req)
	UserID := t.Claims("id")

	var votes, skips, suggestions, invites string
	db.Model(models.Vote{}).Where("user_id = ?", UserID).Count(&votes)
	db.Model(models.Skip{}).Where("user_id = ?", UserID).Count(&skips)
	db.Model(models.Suggestion{}).Where("user_id = ?", UserID).Count(&suggestions)
	db.Model(models.Invite{}).Where("user_id = ?", UserID).Count(&invites)

	results := &models.MySummary{
		votes,
		skips,
		suggestions,
		invites,
	}
	r.JSON(w, http.StatusOK, results)
}

func FilterAllResultsHandler(w http.ResponseWriter, req *http.Request) {

	r := render.New()
	db := middlewares.GetDb(req)

	vars := mux.Vars(req)
	sector := vars["sector"]
	region := vars["region"]

	//Or(User{Name: "jinzhu 2"})
	// TODO: better implementation of filters

	var results []models.FilteredResult
	if sector == "br" || sector == "com" || sector == "ong" || sector == "edu" || sector == "gov" {
		db.Table("users").Select("votes.choice_id, votes.choice_text, count(votes.choice_id) as total").Joins("left join votes on votes.user_id = users.id").Where("users.sector = ?", sector).Group("votes.choice_id").Order("total desc").Scan(&results)
	}

	if region == "norte" {
		db.Table("users").Select("votes.choice_id, votes.choice_text, count(votes.choice_id) as total").Joins("left join votes on votes.user_id = users.id").Where("users.estado in (?)", []string{"AC", "AP", "AM", "PA", "RO", "RR", "TO"}).Group("votes.choice_id").Order("total desc").Scan(&results)
	}
	if region == "nordeste" {
		db.Table("users").Select("votes.choice_id, votes.choice_text, count(votes.choice_id) as total").Joins("left join votes on votes.user_id = users.id").Where("users.estado in (?)", []string{"AL", "BA", "CE", "MA", "PB", "PE", "PI", "RN", "SE"}).Group("votes.choice_id").Order("total desc").Scan(&results)
	}
	if region == "centro" {
		db.Table("users").Select("votes.choice_id, votes.choice_text, count(votes.choice_id) as total").Joins("left join votes on votes.user_id = users.id").Where("users.estado in (?)", []string{"DF", "GO", "MT", "MS"}).Group("votes.choice_id").Order("total desc").Scan(&results)
	}
	if region == "sudeste" {
		db.Table("users").Select("votes.choice_id, votes.choice_text, count(votes.choice_id) as total").Joins("left join votes on votes.user_id = users.id").Where("users.estado in (?)", []string{"ES", "MG", "RJ", "SP"}).Group("votes.choice_id").Order("total desc").Scan(&results)
	}
	if region == "sul" {
		db.Table("users").Select("votes.choice_id, votes.choice_text, count(votes.choice_id) as total").Joins("left join votes on votes.user_id = users.id").Where("users.estado in (?)", []string{"PR", "SC", "RS"}).Group("votes.choice_id").Order("total desc").Scan(&results)
	}

	if results == nil {
		//		r.JSON(w, http.StatusOK, map[string]string{"total": "0"})
		r.JSON(w, http.StatusOK, []string{"empty"})
	} else {
		r.JSON(w, http.StatusOK, results)
	}

}

func MyVotesHandler(w http.ResponseWriter, req *http.Request) {

	r := render.New()
	db := middlewares.GetDb(req)

	t := tauth.Get(req)
	UserID := t.Claims("id")

	user := models.User{ID: int(UserID.(float64))}
	var votes []models.Vote
	db.Model(&user).Related(&votes)

	r.JSON(w, http.StatusOK, votes)
}
