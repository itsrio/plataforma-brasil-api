package controllers

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"

	"github.com/freehaha/token-auth/jwt"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"github.com/joho/godotenv"
	"github.com/mholt/binding"
	"github.com/nucleo-digital/plataforma-brasil-api/middlewares"
	"github.com/nucleo-digital/plataforma-brasil-api/models"
	"github.com/unrolled/render"
)

func AdminDiscussionToggleVisibility(w http.ResponseWriter, req *http.Request) {
	var discussion models.Discussion

	r := render.New()
	db := middlewares.GetDb(req)
	vars := mux.Vars(req)
	id := vars["id"]

	query := db.Find(&discussion, id)
	if query.Error != nil {
		erHJSON(r, w, "Não foi possivel encontrar a discussão", http.StatusNotFound)
	} else {
		discussion.Visible = !discussion.Visible
		db.Save(&discussion)
		r.JSON(w, http.StatusOK,
			map[string]string{
				"msg": fmt.Sprintf("visibilidade da discussão trocada para: %v", discussion.Visible)})
	}
}

func AdminDiscussionCommentToggleVisibility(w http.ResponseWriter, req *http.Request) {
	var dcm models.DiscussionComment

	r := render.New()
	db := middlewares.GetDb(req)
	vars := mux.Vars(req)
	id := vars["id"]

	query := db.Find(&dcm, id)
	if query.Error != nil {
		erHJSON(r, w, "Não foi possivel encontrar o comentario", http.StatusNotFound)
	} else {
		dcm.Visible = !dcm.Visible
		db.Save(&dcm)
		r.JSON(w, http.StatusOK,
			map[string]string{
				"msg": fmt.Sprintf("visibilidade do comentario trocada para: %v", dcm.Visible)})
	}
}

type AdminLoginForm struct {
	Username string
	Password string
}

func AdminUserLoginHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	err = req.ParseForm()
	if err != nil {
		// Handle error
	}

	loginForm := new(AdminLoginForm)
	decoder := schema.NewDecoder()
	// r.PostForm is a map of our POST form values
	err = decoder.Decode(loginForm, req.PostForm)

	if err != nil {
		// Handle error
	}

	user := &models.User{}
	db.Where("email = ?", os.Getenv("ADMIN_EMAIL")).First(&user)
	auth := correct_bcrypt([]byte(user.Password), loginForm.Password)

	if loginForm.Username != os.Getenv("ADMIN_EMAIL") {
		r.JSON(w, http.StatusNotFound, map[string]string{"message": "User not found"})
	} else if auth {
		// remove password from response
		user.Password = ""
		r.JSON(w, http.StatusOK, map[string]string{"access_token": AdminUserGenerateToken(user)})
	} else {
		r.JSON(w, http.StatusNotFound, map[string]string{"message": "User not found"})
	}
}

type UserListResponse struct {
	Users []models.User `json:"users"`
}

func AdminUserListHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)
	users := []models.User{}
	db.Limit(200).Order("created_at desc").Find(&users)
	resp_users := &UserListResponse{users}

	r.JSON(w, http.StatusOK, resp_users)
}

type Choice struct {
	Active                 bool    `json:"active"`
	CreatedAt              string  `json:"created_at"`
	CreatorID              int     `json:"creator_id"`
	Data                   string  `json:"data"`
	ID                     int     `json:"id"`
	ItemID                 string  `json:"item_id"`
	LocalIdentifier        string  `json:"local_identifier"`
	Losses                 int     `json:"losses"`
	Position               string  `json:"position"`
	PromptID               string  `json:"prompt_id"`
	PromptsCount           int     `json:"prompts_count"`
	PromptsOnTheLeftCount  int     `json:"prompts_on_the_left_count"`
	PromptsOnTheRightCount int     `json:"prompts_on_the_right_count"`
	QuestionID             int     `json:"question_id"`
	Ratings                string  `json:"ratings"`
	RequestID              string  `json:"request_id"`
	Score                  float64 `json:"score"`
	Tracking               string  `json:"tracking"`
	UpdatedAt              string  `json:"updated_at"`
	Version                int     `json:"version"`
	Wins                   int     `json:"wins"`
}

type AdminChoicesAOI []struct {
	Choice `json:"choice"`
}

type AdminChoicesList struct {
	Choices []Choice `json:"choices"`
}

type AdminViewChoiceAOI struct {
	Choice `json:"choice"`
}

func AdminChoiceViewHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	v := url.Values{}

	vars := mux.Vars(req)
	id := vars["id"]

	b := PairwiseGetJSON("questions/1/choices/"+fmt.Sprintf("%v", id)+".json", v)
	var f AdminViewChoiceAOI
	err := json.Unmarshal(b, &f)
	if err != nil {
		log.Println("Error decodign choices ", err)
	}

	r.JSON(w, http.StatusOK, f)
}

func AdminChoicesHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	v := url.Values{}
	v.Set("include_inactive", "1")
	b := PairwiseGetJSON("questions/1/choices.json?", v)

	var f AdminChoicesAOI
	err := json.Unmarshal(b, &f)
	if err != nil {
		log.Println("Error decodign choices ", err)
	}

	var g AdminChoicesList
	cc := []Choice{}
	for _, elem := range f {
		cc = append(cc, elem.Choice)
	}
	g.Choices = cc
	r.JSON(w, http.StatusOK, g)
}

func AdminEditChoiceHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()

	vars := mux.Vars(req)
	id := vars["id"]
	ChoiceForm := new(models.AdminParamsChoiceCreate)
	errs := binding.Bind(req, ChoiceForm)
	if errs.Handle(w) {
		return
	}

	v := url.Values{}
	v.Set("choice[visitor_identifier]", ChoiceForm.VisitorIdentifier)
	v.Add("choice[data]", template.HTMLEscapeString(ChoiceForm.Data))
	v.Add("choice[active]", strconv.FormatBool(ChoiceForm.Active))
	v.Add("choice[local_identifier]", ChoiceForm.LocalIdentifier)

	m := PairwisePut("questions/1/choices/"+fmt.Sprintf("%v", id)+".xml?", v)

	ms := &models.ChoiceCreate{
		XmlGetValue(m, "choice.active.#text"),
		XmlGetValue(m, "choice.created-at.#text"),
		XmlGetValue(m, "choice.creator-id.#text"),
		XmlGetValue(m, "choice.data.#text"),
		XmlGetValue(m, "choice.id.#text"),
		XmlGetValue(m, "choice.item-id.#text"),
		XmlGetValue(m, "choice.local-identifier.#text"),
		XmlGetValue(m, "choice.loss-count.#text"),
		XmlGetValue(m, "choice.losses.#text"),
		XmlGetValue(m, "choice.position.#text"),
		XmlGetValue(m, "choice.prompt-id.#text"),
		XmlGetValue(m, "choice.prompts-count.#text"),
		XmlGetValue(m, "choice.prompts-on-the-left-count.#text"),
		XmlGetValue(m, "choice.prompts-on-the-right-count.#text"),
		XmlGetValue(m, "choice.question-id.#text"),
		XmlGetValue(m, "choice.ratings.#text"),
		XmlGetValue(m, "choice.request-id.#text"),
		XmlGetValue(m, "choice.score.#text"),
		XmlGetValue(m, "choice.tracking.#text"),
		XmlGetValue(m, "choice.updated-at.#text"),
		XmlGetValue(m, "choice.votes-count.#text"),
		XmlGetValue(m, "choice.wins.#text"),
	}

	r.JSON(w, http.StatusOK, ms)
}

func AdminNewChoiceHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()

	ChoiceForm := new(models.AdminParamsChoiceCreate)
	errs := binding.Bind(req, ChoiceForm)
	if errs.Handle(w) {
		return
	}

	v := url.Values{}
	v.Set("choice[visitor_identifier]", ChoiceForm.VisitorIdentifier)
	v.Add("choice[data]", template.HTMLEscapeString(ChoiceForm.Data))
	v.Add("choice[active]", strconv.FormatBool(ChoiceForm.Active))
	v.Add("choice[local_identifier]", ChoiceForm.LocalIdentifier)

	m := PairwisePost("questions/1/choices.xml?", v)

	ms := &models.ChoiceCreate{
		XmlGetValue(m, "choice.active.#text"),
		XmlGetValue(m, "choice.created-at.#text"),
		XmlGetValue(m, "choice.creator-id.#text"),
		XmlGetValue(m, "choice.data.#text"),
		XmlGetValue(m, "choice.id.#text"),
		XmlGetValue(m, "choice.item-id.#text"),
		XmlGetValue(m, "choice.local-identifier.#text"),
		XmlGetValue(m, "choice.loss-count.#text"),
		XmlGetValue(m, "choice.losses.#text"),
		XmlGetValue(m, "choice.position.#text"),
		XmlGetValue(m, "choice.prompt-id.#text"),
		XmlGetValue(m, "choice.prompts-count.#text"),
		XmlGetValue(m, "choice.prompts-on-the-left-count.#text"),
		XmlGetValue(m, "choice.prompts-on-the-right-count.#text"),
		XmlGetValue(m, "choice.question-id.#text"),
		XmlGetValue(m, "choice.ratings.#text"),
		XmlGetValue(m, "choice.request-id.#text"),
		XmlGetValue(m, "choice.score.#text"),
		XmlGetValue(m, "choice.tracking.#text"),
		XmlGetValue(m, "choice.updated-at.#text"),
		XmlGetValue(m, "choice.votes-count.#text"),
		XmlGetValue(m, "choice.wins.#text"),
	}

	r.JSON(w, http.StatusOK, ms)
}

func AdminUserCountHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)
	var users_count string
	db.Model(models.User{}).Count(&users_count)
	r.JSON(w, http.StatusOK, map[string]string{"total": users_count})
}

// não está sendo usado
func AdminUserListDownloadHandler(w http.ResponseWriter, req *http.Request) {
	//r := render.New()
	db := middlewares.GetDb(req)
	users := []models.User{}
	db.Order("created_at desc").Find(&users)

	var records [][]string
	for _, obj := range users {
		var record []string
		record = append(record, strconv.Itoa(obj.ID))
		record = append(record, obj.Nome)
		record = append(record, obj.PhotoUrl)
		record = append(record, obj.Email)
		record = append(record, obj.Password)
		record = append(record, obj.DataNascimento)
		record = append(record, obj.Ocupacao)
		record = append(record, obj.Cidade)
		record = append(record, obj.Estado)
		record = append(record, obj.Cpf)
		record = append(record, obj.Descricao)
		record = append(record, obj.Genero)
		record = append(record, obj.FacebookID)
		record = append(record, obj.Sector)
		records = append(records, record)
	}

	//r.JSON(w, http.StatusOK, resp_users)
	RenderCSV(w, records, "users.csv")
}

func RenderCSV(w http.ResponseWriter, records [][]string, filename string) {
	w.Header().Add("Content-Type", "text/csv")
	if len(filename) > 0 {
		w.Header().Add("Content-Disposition", fmt.Sprintf("attachment;filename=%s", filename))
	}
	//	w.Header().Add("Content-Type", "application/force-download")
	encoder := csv.NewWriter(w)
	err := encoder.WriteAll(records)
	if err != nil {
		log.Println(err, "csv encode error")
	}
}

func AdminUserGenerateToken(user *models.User) string {
	admin_jwtst := jwtstore.New("admin_platbr", time.Hour*24)
	t := admin_jwtst.NewToken(user.Email)
	t.SetClaim("email", user.Email).
		SetClaim("facebook_id", user.FacebookID).
		SetClaim("photo_url", user.PhotoUrl).
		SetClaim("id", user.ID).
		SetClaim("nome", user.Nome).
		SetClaim("descricao", user.Descricao).
		SetClaim("exp", time.Now().Add(time.Hour*24).Unix())
	return t.String()
}
