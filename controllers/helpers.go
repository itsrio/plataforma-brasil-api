package controllers

import (
	"net/http"
	"regexp"
	"time"

	"github.com/avelino/slugify"
	"github.com/freehaha/token-auth"
	"github.com/freehaha/token-auth/jwt"
	"github.com/jinzhu/gorm"
	"github.com/nucleo-digital/plataforma-brasil-api/models"
	"github.com/unrolled/render"
)

// buildOffsetAndLimitFromHeader builds the offset and limit from
// request content-range header
func buildOffsetAndLimitFromHeader(headerRangeString string) (o int, l int) {
	var offset = 0
	var limit = 10

	re := regexp.MustCompile(`^(\d+)\-(\d+)`)
	match := re.FindAllStringSubmatch(headerRangeString, 1)

	if match != nil {
		offset = atoi(match[0][1])
		limit = atoi(match[0][2])
	}
	return offset, limit
}

// HANDLERS HELPERS
// asyncRemapCategories remap categories on theme to avoid duplications
func asyncRemapCategories(resourceCategories []models.Category, db *gorm.DB) []models.Category {
	if len(resourceCategories) > 0 {
		remaped := []models.Category{}
		mapChan := make(chan models.Category, len(resourceCategories))

		for _, ct := range resourceCategories {
			go func(cat models.Category) {
				var vct models.Category
				query := db.Where("slug = ?", slugify.Slugify(cat.Name)).First(&vct)

				if query.Error == nil && !db.NewRecord(vct) {
					mapChan <- vct
				} else {
					mapChan <- cat
				}
			}(ct)
		}

		for {
			select {
			case r := <-mapChan:
				remaped = append(remaped, r)
				if len(remaped) == len(resourceCategories) {
					return remaped
				}
			}
		}
		return remaped
	}

	return resourceCategories
}

func erHJSON(r *render.Render, w http.ResponseWriter, emsg string, statusCode int) {
	r.JSON(
		w,
		statusCode,
		map[string]string{"error": emsg},
	)
}

func currentUserID(req *http.Request) int {
	jwtst := jwtstore.New("platbr", time.Hour*24)
	t := tauth.NewTokenAuth(nil, NotAuthorizedHandler, jwtst, &tauth.BearerGetter{})
	token, err := t.Authenticate(req)

	if err == nil {
		return int(token.Claims("id").(float64))
	} else {
		return 0
	}
}
