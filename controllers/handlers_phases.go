package controllers

import (
	"net/http"

	"github.com/mholt/binding"
	"github.com/nucleo-digital/plataforma-brasil-api/middlewares"
	"github.com/nucleo-digital/plataforma-brasil-api/models"
	"github.com/unrolled/render"
)

// NORMAL HANDLERS
// PublishedPhaseHandler get the current phase details
func PublishedPhaseHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)
	phase := models.Phase{}
	db.Limit(1).Find(&phase)

	if db.NewRecord(phase) {
		r.JSON(w, http.StatusNotFound, map[string]string{"error": "nenhuma fase encontrada"})
	} else {
		r.JSON(w, http.StatusOK, phase)
	}
}

// ADMIN HANDLERS
// AdminPhasePublish updates the current phase
func AdminPhasePublish(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)
	phase := new(models.Phase)
	db.Limit(1).Find(&phase)

	if binding.Bind(req, phase).Handle(w) {
		return
	}

	saveQuery := db.Save(&phase)

	if saveQuery.Error != nil {
		r.JSON(w, http.StatusBadRequest, map[string]string{"error": "ocorreu um erro ao tentar salvar a fase"})
	} else {
		r.JSON(w, http.StatusOK, phase)
	}
}
