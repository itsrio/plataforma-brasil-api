package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/freehaha/token-auth"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"github.com/mholt/binding"
	"github.com/nucleo-digital/plataforma-brasil-api/middlewares"
	"github.com/nucleo-digital/plataforma-brasil-api/models"
	"github.com/nucleo-digital/plataforma-brasil-api/serializers"
	"github.com/unrolled/render"
)

// IndexDiscussionHandler display an discussion enabled index
func IndexDiscussionHandler(w http.ResponseWriter, req *http.Request) {
	var discussions []models.Discussion
	var discussionsSerializer []serializers.DiscussionSerializer
	var totalRecords int
	offset, limit := buildOffsetAndLimitFromHeader(req.Header.Get("Content-Range"))

	r := render.New()
	db := middlewares.GetDb(req)

	themeID := atoi(req.FormValue("theme_id"))
	orderParam := req.FormValue("order")
	flagParam := req.FormValue("flagged")
	categoryParam := req.FormValue("category")
	authorParam := req.FormValue("author")

	super_query := db.Select("DISTINCT discussions.*").Scopes(
		models.DiscussionsDefaultJoins(),
		models.HideOrShowDiscussionsWhereFlagedFor(flagParam, currentUserID(req)),
		models.DiscussionsByCategory(categoryParam),
		models.DiscussionsByThemeId(themeID),
		models.DiscussionsWhereAuthorMatches(authorParam),
		models.DiscussionsByOrder(orderParam)).
		Where("discussions.visible is true")

	super_query.Model(&models.Discussion{}).Select("COUNT(DISTINCT discussions.id)").Row().Scan(&totalRecords)
	query := super_query.Limit(fmt.Sprintf("%d,%d", offset, limit)).Find(&discussions)
	if query.Error != nil {
		erHJSON(r, w, "Não foi possivel listar as discussões tente novamente mais tarde", http.StatusNotFound)
	} else {
		for _, discussion := range discussions {
			var ds serializers.DiscussionSerializer
			ds.BuildFromResource(&discussion, db, currentUserID(req))
			discussionsSerializer = append(discussionsSerializer, ds)
		}

		w.Header().Set(
			"Content-Range",
			fmt.Sprintf("%d-%d/%d", offset, limit, totalRecords),
		)
		w.Header().Set("Range-unit", "discussion")
		r.JSON(w, http.StatusPartialContent, discussionsSerializer)
	}
}

// ShowDiscussionHandler display an discussion details
func ShowDiscussionHandler(w http.ResponseWriter, req *http.Request) {
	var discussion models.Discussion
	var discussionSerializer serializers.DiscussionSerializer

	r := render.New()
	db := middlewares.GetDb(req)
	vars := mux.Vars(req)
	id := vars["id"]

	query := db.Find(&discussion, id)
	if query.Error != nil {
		erHJSON(r, w, "Não foi possivel encontrar a discussão", http.StatusNotFound)
	} else {
		discussionSerializer.BuildFromResource(&discussion, db, currentUserID(req))
		r.JSON(w, http.StatusOK, discussionSerializer)
	}
}

// LikeDiscussionHandler handles with likes on an discussion
func LikeDiscussionHandler(w http.ResponseWriter, req *http.Request) {
	var discussion models.Discussion
	var discussionLike models.DiscussionLike

	r := render.New()
	db := middlewares.GetDb(req)
	vars := mux.Vars(req)
	id := vars["id"]

	findDiscussionQ := db.Find(&discussion, "id = ?", id)
	if findDiscussionQ.Error != nil {
		erHJSON(r, w, "Não foi possivel encontrar a discussão", http.StatusNotFound)
		return
	}

	t := tauth.Get(req)
	UserID := t.Claims("id").(float64)
	findDLikeQ := db.Find(
		&discussionLike,
		"discussion_id = ? AND user_id = ?",
		discussion.ID, UserID,
	)

	if findDLikeQ.Error != nil {
		discussionLike.DiscussionID = discussion.ID
		discussionLike.UserID = int(UserID)
		discussionLike.IPMask = req.RemoteAddr
		db.Save(&discussionLike)
		discussionLike.CreateDiscussionLikeNotification(db)

		r.JSON(w, http.StatusCreated, map[string]string{"msg": "liked"})
	} else {
		erHJSON(r, w, "Você já deu like aqui :)", http.StatusBadRequest)
		return
	}
}

// DisLikeDiscussionHandler handles with likes on an discussion
func DisLikeDiscussionHandler(w http.ResponseWriter, req *http.Request) {
	var discussion models.Discussion
	var discussionDisLike models.DiscussionDislike

	r := render.New()
	db := middlewares.GetDb(req)
	vars := mux.Vars(req)
	id := vars["id"]

	findDiscussionQ := db.Find(&discussion, "id = ?", id)
	if findDiscussionQ.Error != nil {
		erHJSON(r, w, "Não foi possivel encontrar a discussão", http.StatusNotFound)
		return
	}

	t := tauth.Get(req)
	UserID := t.Claims("id").(float64)
	findDLikeQ := db.Find(
		&discussionDisLike,
		"discussion_id = ? AND user_id = ?",
		discussion.ID, UserID,
	)

	if findDLikeQ.Error != nil {
		discussionDisLike.DiscussionID = discussion.ID
		discussionDisLike.UserID = int(UserID)
		discussionDisLike.IPMask = req.RemoteAddr
		db.Save(&discussionDisLike)
		discussionDisLike.CreateDiscussionDislikeNotification(db)

		r.JSON(w, http.StatusCreated, map[string]string{"msg": "unliked"})
	} else {
		erHJSON(r, w, "Você já deu dislike aqui :)", http.StatusBadRequest)
		return
	}
}

// CreateDiscussionHandler handles with creation of an discussion
func CreateDiscussionHandler(w http.ResponseWriter, req *http.Request) {
	var discussionSerializer serializers.DiscussionSerializer

	r := render.New()
	db := middlewares.GetDb(req)
	discussion := &models.Discussion{}
	theme := &models.Theme{}

	if binding.Bind(req, discussion).Handle(w) {
		return
	}

	discussion.Categories = asyncRemapCategories(discussion.Categories, db)

	for _, cat := range discussion.Categories {
		var totalCount int

		if db.NewRecord(cat) {
			erHJSON(r, w, fmt.Sprintf("Categoria %s não encontrada!", cat), http.StatusBadRequest)
			return
		}

		db.Table("category_themes").Where("theme_id = ? and category_id = ?", discussion.ThemeID, cat.ID).Count(&totalCount)
		if totalCount < 1 {
			erHJSON(r, w, fmt.Sprintf("Categoria %s não faz parte do tema enviado!", cat), http.StatusBadRequest)
			return
		}
	}

	if db.Where("id = ?", discussion.ThemeID).First(&theme).RecordNotFound() {
		erHJSON(r, w, "Tema invalido!", http.StatusBadRequest)
		return

	}
	UserID := currentUserID(req)
	discussion.UserID = UserID
	discussion.Visible = true
	saveQuery := db.Create(&discussion)

	if saveQuery.Error != nil {
		erHJSON(r, w, "Ocorreu um erro ao tentar salvar a discussão.", http.StatusBadRequest)
	} else {
		discussion.CreateTopic(db)
		discussionSerializer.BuildFromResource(discussion, db, UserID)
		r.JSON(w, http.StatusOK, discussionSerializer)
	}
}

// getDiscussionsCategories load categories from a l	ds.UserCommentsist of discussions
func getDiscussionsCategories(collection []models.Discussion, db *gorm.DB) []models.Discussion {
	for i, _ := range collection {
		getDiscussionCategories(&collection[i], db)
	}

	return collection
}

// getDiscussionCategories load categories for an single discussion
func getDiscussionCategories(resource *models.Discussion, db *gorm.DB) *models.Discussion {
	var categories []models.Category

	db.Model(&resource).Association("Categories").Find(&categories)
	resource.Categories = categories

	return resource
}

func atoi(str string) int {
	n, _ := strconv.Atoi(str)
	return n
}
