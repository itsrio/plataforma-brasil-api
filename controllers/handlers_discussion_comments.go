package controllers

import (
	"fmt"
	"net/http"

	"github.com/freehaha/token-auth"
	"github.com/gorilla/mux"
	"github.com/mholt/binding"
	"github.com/nucleo-digital/plataforma-brasil-api/middlewares"
	"github.com/nucleo-digital/plataforma-brasil-api/models"
	"github.com/nucleo-digital/plataforma-brasil-api/serializers"
	"github.com/unrolled/render"
)

// ShowCommentsDiscussionHandler find and show a list of comments on discussion
func ShowCommentsDiscussionHandler(w http.ResponseWriter, req *http.Request) {
	var dsComments []models.DiscussionComment
	var dcmSerializer []serializers.DiscussionCommentSerializer
	var totalRecords int
	offset, limit := buildOffsetAndLimitFromHeader(req.Header.Get("Content-Range"))

	r := render.New()
	db := middlewares.GetDb(req)
	vars := mux.Vars(req)
	id := vars["id"]
	flagParam := req.FormValue("flagged")

	super_query := db.Scopes(models.HideOrShowCommentsFor(flagParam, currentUserID(req))).
		Where("discussion_comments.discussion_id = ? AND discussion_comments.visible is true", id).
		Order("discussion_comments.created_at desc")

	super_query.Model(&models.DiscussionComment{}).Count(&totalRecords)
	super_query.Limit(fmt.Sprintf("%d,%d", offset, limit)).
		Find(&dsComments)

	for _, comment := range dsComments {
		var ds serializers.DiscussionCommentSerializer
		ds.BuildFromResource(&comment, db, currentUserID(req))
		dcmSerializer = append(dcmSerializer, ds)
	}

	w.Header().Set(
		"Content-Range",
		fmt.Sprintf("%d-%d/%d", offset, limit, totalRecords),
	)
	w.Header().Set("Range-unit", "discussion_comment")

	r.JSON(w, http.StatusOK, dcmSerializer)
}

// CreateCommentDiscussionHandler create an comment on discussion
func CreateCommentDiscussionHandler(w http.ResponseWriter, req *http.Request) {
	var discussion models.Discussion
	var dcmSerializer serializers.DiscussionCommentSerializer

	disComment := &models.DiscussionComment{}

	r := render.New()
	db := middlewares.GetDb(req)
	vars := mux.Vars(req)
	id := vars["id"]

	findDiscussionQ := db.Find(&discussion, "id = ?", id)
	if findDiscussionQ.Error != nil {
		erHJSON(r, w, "Não foi possivel encontrar a discussão", http.StatusNotFound)
		return
	}

	UserID := currentUserID(req)
	if binding.Bind(req, disComment).Handle(w) {
		return
	}

	disComment.DiscussionID = discussion.ID
	disComment.UserID = UserID
	disComment.Visible = true
	createQuery := db.Create(&disComment)
	if createQuery.Error == nil {
		disComment.CreateTopic(db)
		dcmSerializer.BuildFromResource(disComment, db, UserID)
		r.JSON(w, http.StatusCreated, dcmSerializer)
	} else {
		erHJSON(r, w, "Não foi possivel criar o seu comentario, tente novamente mais tarde.", http.StatusBadRequest)
	}
}

// LikeCommentDiscussionHandler handles with likes on an discussion
func LikeCommentDiscussionHandler(w http.ResponseWriter, req *http.Request) {
	var discussion models.Discussion
	var dcm models.DiscussionComment
	var dlike models.DiscussionCommentLike

	r := render.New()
	db := middlewares.GetDb(req)
	vars := mux.Vars(req)
	id := vars["id"]
	commentID := vars["comment_id"]
	UserID := currentUserID(req)

	findDiscussionQ := db.Find(&discussion, "id = ?", id)
	if findDiscussionQ.Error != nil {
		erHJSON(r, w, "Não foi possivel encontrar a discussão", http.StatusNotFound)
		return
	}

	findCommentQ := db.Find(&dcm, "id = ?", commentID)
	if findCommentQ.Error != nil {
		erHJSON(r, w, "Não foi possivel encontrar o comentario", http.StatusNotFound)
		return
	}

	findDLikeQ := db.Find(
		&dlike,
		"discussion_comment_id = ? AND user_id = ?",
		dcm.ID, UserID,
	)

	if findDLikeQ.Error != nil {
		dlike.DiscussionCommentID = dcm.ID
		dlike.UserID = int(UserID)
		dlike.IPMask = req.RemoteAddr
		db.Save(&dlike)
		dlike.CreateDiscussionCommentNotification(db)

		r.JSON(w, http.StatusCreated, map[string]string{"msg": "liked"})
	} else {
		erHJSON(r, w, "Você já deu like aqui :)", http.StatusBadRequest)
		return
	}
}

// DisLikeDiscussionHandler handles with likes on an discussion
func DisLikeCommentDiscussionHandler(w http.ResponseWriter, req *http.Request) {
	var discussion models.Discussion
	var dcm models.DiscussionComment
	var dlike models.DiscussionCommentDisLike

	r := render.New()
	db := middlewares.GetDb(req)
	vars := mux.Vars(req)
	id := vars["id"]
	commentID := vars["comment_id"]

	findDiscussionQ := db.Find(&discussion, "id = ?", id)
	if findDiscussionQ.Error != nil {
		erHJSON(r, w, "Não foi possivel encontrar a discussão", http.StatusNotFound)
		return
	}

	findCommentQ := db.Find(&dcm, "id = ?", commentID)
	if findCommentQ.Error != nil {
		erHJSON(r, w, "Não foi possivel encontrar o comentario", http.StatusNotFound)
		return
	}

	t := tauth.Get(req)
	UserID := t.Claims("id").(float64)
	findDLikeQ := db.Find(
		&dlike,
		"discussion_comment_id = ? AND user_id = ?",
		dcm.ID, UserID,
	)

	if findDLikeQ.Error != nil {
		dlike.DiscussionCommentID = dcm.ID
		dlike.UserID = int(UserID)
		dlike.IPMask = req.RemoteAddr
		db.Save(&dlike)
		dlike.CreateDiscussionCommentNotification(db)

		r.JSON(w, http.StatusCreated, map[string]string{"msg": "disliked"})
	} else {
		erHJSON(r, w, "Você já deu dislike aqui :)", http.StatusBadRequest)
		return
	}
}

func AdminDiscussionCommentsIndex(w http.ResponseWriter, req *http.Request) {
	var dsComments []models.DiscussionComment
	var dcmSerializer []serializers.DiscussionCommentSerializer
	var totalRecords int
	offset, limit := buildOffsetAndLimitFromHeader(req.Header.Get("Content-Range"))
	limit = 1000
	r := render.New()
	db := middlewares.GetDb(req)

	super_query := db.Model(&models.DiscussionComment{}).Count(&totalRecords)
	super_query.Order("discussion_comments.created_at desc").Limit(fmt.Sprintf("%d,%d", offset, limit)).
		Find(&dsComments)

	for _, comment := range dsComments {
		var ds serializers.DiscussionCommentSerializer
		ds.BuildFromResource(&comment, db, currentUserID(req))
		dcmSerializer = append(dcmSerializer, ds)
	}

	w.Header().Set(
		"Content-Range",
		fmt.Sprintf("%d-%d/%d", offset, limit, totalRecords),
	)
	w.Header().Set("Range-unit", "discussion_comment")

	r.JSON(w, http.StatusOK, dcmSerializer)
}
