package controllers

import (
	"github.com/mitchellh/goamz/aws"
	"github.com/mitchellh/goamz/s3"

	"code.google.com/p/go.crypto/bcrypt"
	"github.com/freehaha/token-auth"
	"github.com/freehaha/token-auth/jwt"

	"github.com/joho/godotenv"

	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/mholt/binding"
	"github.com/nucleo-digital/plataforma-brasil-api/middlewares"
	"github.com/nucleo-digital/plataforma-brasil-api/models"
	"github.com/nucleo-digital/plataforma-brasil-api/serializers"
	"github.com/unrolled/render"

	"crypto/tls"
	"fmt"
	"log"
	"math/rand"
	"net"
	"net/mail"
	"net/smtp"

	"os"
)

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		rand.Seed(time.Now().UTC().UnixNano())
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

// TODO: remove call to this func and replace to use services
func SendEmail(subj string, body string, from mail.Address, to mail.Address, x http.ResponseWriter) {
	r := render.New()
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	// Connect to the SMTP Server
	servername := os.Getenv("SMTP_SERVER")

	host, _, _ := net.SplitHostPort(servername)

	auth := smtp.PlainAuth("", os.Getenv("SMTP_USER"), os.Getenv("SMTP_PASS"), host)

	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         host,
	}

	// Setup headers
	headers := make(map[string]string)
	headers["MIME-version"] = "1.0"
	headers["Content-Type"] = "text/html; charset=\"UTF-8\""
	headers["From"] = from.String()
	headers["To"] = to.String()
	headers["Subject"] = subj

	// Setup message
	message := ""
	for k, v := range headers {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + body

	// Here is the key, you need to call tls.Dial instead of smtp.Dial
	// for smtp servers running on 465 that require an ssl connection
	// from the very beginning (no starttls)
	conn, err := tls.Dial("tcp", servername, tlsconfig)
	if err != nil {
		r.JSON(x, http.StatusInternalServerError, err)
	}

	c, err := smtp.NewClient(conn, host)
	if err != nil {
		r.JSON(x, http.StatusInternalServerError, err)
	}

	// Auth
	if err = c.Auth(auth); err != nil {
		r.JSON(x, http.StatusInternalServerError, err)
	}

	// To && From
	if err = c.Mail(from.Address); err != nil {
		r.JSON(x, http.StatusInternalServerError, err)
	}

	if err = c.Rcpt(to.Address); err != nil {
		r.JSON(x, http.StatusInternalServerError, err)
	}

	// Data
	w, err := c.Data()
	if err != nil {
		r.JSON(x, http.StatusInternalServerError, err)
	}

	_, err = w.Write([]byte(message))
	if err != nil {
		r.JSON(x, http.StatusInternalServerError, err)
	}

	err = w.Close()
	if err != nil {
		r.JSON(x, http.StatusInternalServerError, err)
	}

	c.Quit()

}

// https://gist.github.com/chrisgillis/10888032
func UserInviteHandler(x http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)

	inviteForm := new(models.UserInvite)
	errs := binding.Bind(req, inviteForm)
	if errs.Handle(x) {
		return
	}

	from := mail.Address{"Plataforma Brasil", "nao-responda@plataformabrasil.org.br"}
	to := mail.Address{"", inviteForm.Email}
	subj := "Você foi convidado para participar da Plataforma Brasil"
	body := inviteForm.Message

	SendEmail(subj, body, from, to, x)

	//	if err == nil {
	t := tauth.Get(req)
	UserID := t.Claims("id").(float64)
	iv := models.Invite{UserID: int(UserID), EmailTo: to.String(), Message: body}

	db.Create(&iv)
	r.JSON(x, http.StatusOK, map[string]string{"message": "Mensagem enviada com sucesso."})
	//	}
}

// We expect a multipart/form-data upload with
// a file field named 'data'
type MultipartForm struct {
	Data *multipart.FileHeader `json:"data"`
}

func (f *MultipartForm) FieldMap() binding.FieldMap {
	return binding.FieldMap{
		&f.Data: "data",
	}
}

// Handlers are still clean and simple
func UserAvatarHandler(resp http.ResponseWriter, req *http.Request) {
	multipartForm := new(MultipartForm)
	errs := binding.Bind(req, multipartForm)
	if errs.Handle(resp) {
		return
	}

	// To access the file data you need to Open the file
	// handler and read the bytes out.
	var fh io.ReadCloser
	var err error
	if fh, err = multipartForm.Data.Open(); err != nil {
		http.Error(resp,
			fmt.Sprint("Error opening Mime::Data %+v", err),
			http.StatusInternalServerError)
		return
	}
	defer fh.Close()

	auth, err := aws.EnvAuth()

	if err != nil {
		log.Fatal(err)
	}
	client := s3.New(auth, aws.SAEast)
	b := client.Bucket("platbr-profile-images")

	data, err := ioutil.ReadAll(fh)
	filetype := http.DetectContentType(data)
	err = b.Put(multipartForm.Data.Filename, data, filetype, s3.PublicRead)
	if err != nil {
		panic(err.Error())
	}

	r := render.New()
	db := middlewares.GetDb(req)
	t := tauth.Get(req)
	UserID := t.Claims("id").(float64)

	user := models.User{ID: int(UserID)}
	new_url := b.URL(multipartForm.Data.Filename)
	db.First(&user).Update("photo_url", new_url)
	r.JSON(resp, http.StatusOK, map[string]string{"photo_url": new_url})
}

func UserConfigHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)
	t := tauth.Get(req)
	UserID := t.Claims("id").(float64)

	configs := []models.Configuration{}
	if err := db.Where("user_id = ?", UserID).Find(&configs).Error; err != nil {
		r.JSON(w, http.StatusOK, err)
		return
	}

	r.JSON(w, http.StatusOK, configs)
}

func UserConfigCreateHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)
	t := tauth.Get(req)
	UserID := t.Claims("id").(float64)

	ConfigForm := new(models.Configuration)
	errs := binding.Bind(req, ConfigForm)
	if errs.Handle(w) {
		r.JSON(w, http.StatusInternalServerError, errs)
	}

	config := models.Configuration{}
	if err := db.Where(models.Configuration{UserID: int(UserID), Key: ConfigForm.Key}).First(&config).Error; err != nil {
		config = models.Configuration{UserID: int(UserID), Key: ConfigForm.Key, Value: ConfigForm.Value}
		db.Create(&config)
	} else {
		db.Model(&config).Update("value", ConfigForm.Value)
	}

	r.JSON(w, http.StatusOK, config)
}

func DiscussionsFromUserHandler(w http.ResponseWriter, req *http.Request) {
	var discussions []models.Discussion
	var discussionsSerializer []serializers.DiscussionSerializer

	r := render.New()
	db := middlewares.GetDb(req)
	vars := mux.Vars(req)
	user_id := vars["id"]

	query := db.Where("user_id = ?", user_id).Find(&discussions)
	if query.Error != nil {
		erHJSON(r, w, "Não foi possivel encontrar a discussão", http.StatusNotFound)
	} else {
		for _, discussion := range discussions {
			var ds serializers.DiscussionSerializer
			ds.BuildFromResource(&discussion, db, currentUserID(req))
			discussionsSerializer = append(discussionsSerializer, ds)
		}
		r.JSON(w, http.StatusOK, discussionsSerializer)
	}

}

func ProfileHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	vars := mux.Vars(req)
	id := vars["id"]
	db := middlewares.GetDb(req)

	user := models.User{}
	query := db.First(&user, id)

	if query.Error != nil {
		r.JSON(w, http.StatusNotFound, map[string]string{"message": "Usuário não existe."})
	} else {
		user.Password = ""

		config := models.Configuration{}
		db.Where(models.Configuration{UserID: atoi(id), Key: "privacy"}).First(&config)
		if config.Value == "private" {
			user.Email = ""
			user.DataNascimento = ""
			user.Ocupacao = ""
			user.Cidade = ""
			user.Estado = ""
			user.Cpf = ""
			user.Descricao = ""
			user.Genero = ""
			user.FacebookID = ""
		}

		r.JSON(w, http.StatusOK, user)
	}
}

func UserHandler(w http.ResponseWriter, req *http.Request) {
	db := middlewares.GetDb(req)
	r := render.New()

	t := tauth.Get(req)
	email := t.Claims("email")

	user := models.User{}
	db.Where("email = ?", email).First(&user)

	user.Password = ""
	r.JSON(w, http.StatusOK, user)
}

func UserCreateHandler(w http.ResponseWriter, req *http.Request) {
	db := middlewares.GetDb(req)
	r := render.New()

	userForm := new(models.User)
	errs := binding.Bind(req, userForm)
	if errs.Handle(w) {
		return
	}

	userForm.Password = string(hash_bcrypt(string(userForm.Password)))
	var count int
	if userForm.FacebookID == "" {
		db.Where("email = ?", userForm.Email).Or("cpf = ?", userForm.Cpf).Find(&models.User{}).Count(&count)
	} else {
		db.Where("email = ?", userForm.Email).Find(&models.User{}).Count(&count)
	}

	if count > 0 {
		r.JSON(w, http.StatusOK, map[string]string{"message": "Email or CPF já existe."})
	} else {
		db.Create(&userForm)

		// remove password from response
		userForm.Password = ""
		r.JSON(w, http.StatusOK, map[string]string{"token": UserGenerateToken(userForm)})
	}
}

func UserEditHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)

	t := tauth.Get(req)
	email := t.Claims("email")

	userForm := new(models.User)
	errs := binding.Bind(req, userForm)
	if errs.Handle(w) {
		return
	}
	user := models.User{}
	db.Where("email = ?", email).First(&user)

	if user.ID < 1 {
		r.JSON(w, http.StatusNotFound, map[string]string{"message": "Email not exists"})
	} else {
		userForm.ID = user.ID
		//		if userForm.Password != "" {
		//			userForm.Password = string(hash_bcrypt(string(userForm.Password)))
		//		}

		userForm.Password = user.Password
		userForm.Cpf = user.Cpf
		userForm.Email = user.Email
		userForm.Sector = user.Sector
		db.Save(&userForm)
		r.JSON(w, http.StatusOK, userForm)
	}
}

// Check if a given password is correct, for a given bcrypt hash
func correct_bcrypt(hash []byte, password string) bool {
	// prevents timing attack
	return bcrypt.CompareHashAndPassword(hash, []byte(password)) == nil
}

// Hash the password with bcrypt
func hash_bcrypt(password string) []byte {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		panic("Permissions: bcrypt password hashing unsuccessful")
	}
	return hash
}

func UserResetPasswordHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)

	vars := mux.Vars(req)
	email := vars["email"]

	user := models.User{}
	db.Where("email = ?", email).First(&user)

	reset_code := randSeq(85)
	user.ResetCode = reset_code
	db.Save(&user)

	from := mail.Address{"Plataforma Brasil", "nao-responda@plataformabrasil.org.br"}
	to := mail.Address{"", email}
	subj := "Sua nova senha"
	body := "Olá, " + user.Nome
	body += "<br><br>Clique no link abaixo para criar uma nova senha de acesso para a Plataforma Brasil:"
	body += "<br><br><a href='https://plataformabrasil.org.br/?/nova-senha/" + reset_code + "'>https://plataformabrasil.org.br/?/nova-senha/" + reset_code + "</a>"

	SendEmail(subj, body, from, to, w)

	r.JSON(w, http.StatusOK, map[string]string{"message": "Pedido realizado com sucesso, verifique seu email."})
}

func UserChangePasswordHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)

	resetForm := new(models.ResetPassword)
	errs := binding.Bind(req, resetForm)
	if errs.Handle(w) {
		return
	}

	user := models.User{}
	db.Where("reset_code = ?", resetForm.ResetCode).First(&user)

	if user.ResetCode != "" {
		resetForm.Password = string(hash_bcrypt(string(resetForm.Password)))
		user.Password = resetForm.Password
		user.ResetCode = ""

		db.Save(&user)
		r.JSON(w, http.StatusOK, map[string]string{"message": "Senha alterada com sucesso! Você está sendo redirecionado para o login..."})
	} else {
		r.JSON(w, http.StatusInternalServerError, map[string]string{"message": "Código para nova senha inválido."})
	}
}

func UserLoginHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)

	loginForm := new(models.Login)
	errs := binding.Bind(req, loginForm)
	if errs.Handle(w) {
		return
	}

	user := models.User{}
	db.Where("email = ?", loginForm.Email).First(&user)

	auth := correct_bcrypt([]byte(user.Password), loginForm.Password)
	if auth {
		// remove password from response
		user.Password = ""
		r.JSON(w, http.StatusOK, map[string]string{"token": UserGenerateToken(&user)})
	} else {
		r.JSON(w, http.StatusNotFound, map[string]string{"message": "User not found"})
	}
}

func UserGenerateToken(user *models.User) string {
	jwtst := jwtstore.New("platbr", time.Hour*24)
	t := jwtst.NewToken(user.Email)
	t.SetClaim("email", user.Email).
		SetClaim("facebook_id", user.FacebookID).
		SetClaim("photo_url", user.PhotoUrl).
		SetClaim("id", user.ID).
		SetClaim("nome", user.Nome).
		SetClaim("descricao", user.Descricao).
		SetClaim("exp", time.Now().Add(time.Hour*24).Unix())
	return t.String()
}
