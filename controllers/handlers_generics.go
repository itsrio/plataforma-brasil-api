package controllers

import (
	"github.com/mitchellh/goamz/aws"
	"github.com/mitchellh/goamz/s3"
	"github.com/nucleo-digital/plataforma-brasil-api/middlewares"
	"github.com/nucleo-digital/plataforma-brasil-api/models"

	fb "github.com/huandu/facebook"
	"github.com/mholt/binding"
	"github.com/unrolled/render"

	"log"
	"net/http"
	"strconv"
	"time"
)

func SiteConfigHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)

	configs := []models.SiteConfiguration{}
	if err := db.Find(&configs).Error; err != nil {
		r.JSON(w, http.StatusOK, err)
		return
	}

	r.JSON(w, http.StatusOK, configs)
}

func SiteConfigCreateHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	db := middlewares.GetDb(req)

	ConfigForm := new(models.SiteConfiguration)
	errs := binding.Bind(req, ConfigForm)
	if errs.Handle(w) {
		r.JSON(w, http.StatusInternalServerError, errs)
	}

	config := models.SiteConfiguration{}
	if err := db.Where(models.SiteConfiguration{Key: ConfigForm.Key}).First(&config).Error; err != nil {
		config = models.SiteConfiguration{Key: ConfigForm.Key, Value: ConfigForm.Value}
		db.Create(&config)
	} else {
		db.Model(&config).Update("value", ConfigForm.Value)
	}

	r.JSON(w, http.StatusOK, config)
}

func AmazonS3TempUrlHandler(w http.ResponseWriter, req *http.Request) {

	auth, err := aws.EnvAuth()

	if err != nil {
		log.Fatal(err)
	}
	client := s3.New(auth, aws.SAEast)
	b := client.Bucket("platbr-profile-images")

	t_plus := time.Now()
	t_plus = t_plus.Add(time.Duration(5 * time.Minute))
	url := b.SignedURL("/teste.png", t_plus)
	r := render.New()
	r.JSON(w, http.StatusOK, map[string]string{"temp_url": url})
}

type FacebookUser struct {
	Name        string
	Timezone    string
	UpdatedTime string
	ID          string
	Gender      string
	LastName    string
	Link        string
	Locale      string
	Verified    bool
	Email       string
	FirstName   string
}

func FacebookDetailsHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	accessToken := req.FormValue("code")
	db := middlewares.GetDb(req)

	if accessToken == "" {
		log.Printf("long lived accessToken is empty")
		r.JSON(w, http.StatusOK, map[string]string{"message": "Access Token vazio."})
		return
	}

	res, err := fb.Get("/me", fb.Params{
		"access_token": accessToken,
	})

	if err != nil {
		// err can be an facebook API error.
		// if so, the Error struct contains error details.
		if e, ok := err.(*fb.Error); ok {
			log.Printf("facebook error. [message:%v] [type:%v] [code:%v] [subcode:%v]",
				e.Message, e.Type, e.Code, e.ErrorSubcode)
			r.JSON(w, http.StatusOK, map[string]string{"message": "facebook error. [message:" + e.Message + "] [type:" + e.Type + "] [code:" + strconv.Itoa(e.Code) + "] [subcode:" + strconv.Itoa(e.ErrorSubcode) + "]"})
			return
		}
		return
	}

	fb_user := &FacebookUser{}
	res.Decode(fb_user)

	user := &models.User{}
	db.Where("email = ?", fb_user.Email).First(&user)

	// when user was registered with email before facebook
	if user.FacebookID == "" {
		db.Model(models.User{}).Where("email = ?", fb_user.Email).Update("facebook_id", fb_user.ID)
	}

	if user.Email != "" {
		user.Password = ""
		r.JSON(w, http.StatusOK, map[string]string{"token": UserGenerateToken(user)})
	} else {
		r.JSON(w, http.StatusOK, fb_user)
	}
}

func NotAuthorizedHandler(w http.ResponseWriter, req *http.Request) {
	r := render.New()
	r.JSON(w, http.StatusUnauthorized, map[string]string{"message": "unauthorized"})
}
