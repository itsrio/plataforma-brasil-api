package controllers

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/nucleo-digital/plataforma-brasil-api/middlewares"
	"github.com/nucleo-digital/plataforma-brasil-api/models"
	"github.com/nucleo-digital/plataforma-brasil-api/serializers"
	"github.com/unrolled/render"
)

// ShowDiscussionTopicNotificationsHandler display an discussion topic notification
// and turn to Opened
func ShowDiscussionTopicNotificationsHandler(w http.ResponseWriter, req *http.Request) {
	var discussion models.Discussion
	var discussionTopic models.DiscussionTopic
	var discussionTopicNotification models.DiscussionTopicNotification
	var discussionTopicNotificationsSerializer serializers.DiscussionTopicNotificationSerializer

	r := render.New()
	db := middlewares.GetDb(req)
	currentUserId := currentUserID(req)
	vars := mux.Vars(req)
	id := vars["id"]

	db.Model(&models.DiscussionTopic{}).Find(&discussionTopicNotification, id)
	db.Model(&discussionTopicNotification).Related(&discussionTopic)
	db.Model(&discussionTopic).Related(&discussion)

	if currentUserId != discussion.UserID {
		erHJSON(r, w, "Não foi possivel encontrar a notificação, tente novamente mais tarde", http.StatusUnauthorized)
		return
	}

	discussionTopicNotification.Opened = true
	db.Save(&discussionTopicNotification)

	discussionTopicNotificationsSerializer.BuildFromResource(&discussionTopicNotification, db, currentUserId)

	r.JSON(w, http.StatusOK, discussionTopicNotificationsSerializer)
}

// IndexDiscussionTopicNotificationsHandler display an discussion topic_notifications
func IndexDiscussionTopicNotificationsHandler(w http.ResponseWriter, req *http.Request) {
	var discussionIds []int
	var discussionTopicsIds []int
	var discussionTopicNotifications []models.DiscussionTopicNotification
	var discussionTopicNotificationsSerializers []serializers.DiscussionTopicNotificationSerializer
	var totalRecords int

	offset, limit := buildOffsetAndLimitFromHeader(req.Header.Get("Content-Range"))

	r := render.New()
	db := middlewares.GetDb(req)
	currentUserId := currentUserID(req)

	db.Model(&models.Discussion{}).Where("user_id = ?", currentUserId).Count(&totalRecords)
	db.Model(&models.Discussion{}).Where("user_id = ?", currentUserId).Pluck("id", &discussionIds)
	db.Model(&models.DiscussionTopic{}).Where("discussion_id in (?)", discussionIds).Pluck("id", &discussionTopicsIds)
	query := db.Limit(fmt.Sprintf("%d,%d", offset, limit)).
		Order("id DESC").
		Find(&discussionTopicNotifications, "discussion_topic_id in (?)", discussionTopicsIds)

	if query.Error != nil {
		erHJSON(r, w, "Não foi possivel listar as notificações para as discussões tente novamente mais tarde", http.StatusNotFound)
	} else {
		for _, dtn := range discussionTopicNotifications {
			var dtns serializers.DiscussionTopicNotificationSerializer
			dtns.BuildFromResource(&dtn, db, currentUserId)
			discussionTopicNotificationsSerializers = append(discussionTopicNotificationsSerializers, dtns)
		}

		w.Header().Set(
			"Content-Range",
			fmt.Sprintf("%d-%d/%d", offset, limit, totalRecords),
		)
		w.Header().Set("Range-unit", "items")
		r.JSON(w, http.StatusPartialContent, discussionTopicNotificationsSerializers)
	}
}

// IndexDiscussionCommentTopicNotificationsHandler display an discussion comment topic notifications list
func IndexDiscussionCommentTopicNotificationsHandler(w http.ResponseWriter, req *http.Request) {
	var discussionCommentsIds []int
	var discussionCommentTopicIds []int
	var discussionCommentTopicNotifications []models.DiscussionCommentTopicNotification
	var discussionCommentTopicNotificationsSerializers []serializers.DiscussionCommentTopicNotificationsSerializer
	var totalRecords int

	offset, limit := buildOffsetAndLimitFromHeader(req.Header.Get("Content-Range"))

	r := render.New()
	db := middlewares.GetDb(req)
	currentUserId := currentUserID(req)

	db.Model(&models.DiscussionComment{}).Where("user_id = ?", currentUserId).Count(&totalRecords)
	db.Model(&models.DiscussionComment{}).Where("user_id = ?", currentUserId).Pluck("id", &discussionCommentsIds)
	db.Model(&models.DiscussionCommentTopic{}).Where("discussion_comment_id  in(?)", discussionCommentsIds).Pluck("id", &discussionCommentTopicIds)
	query := db.Limit(fmt.Sprintf("%d,%d", offset, limit)).
		Order("id DESC").
		Find(&discussionCommentTopicNotifications, "discussion_comment_topic_id in(?)", discussionCommentTopicIds)

	if query.Error != nil {
		erHJSON(r, w, "Não foi possivel listar as notificações para os comentarios das discussões tente novamente mais tarde", http.StatusNotFound)
	} else {
		for _, dtcn := range discussionCommentTopicNotifications {
			var dtcns serializers.DiscussionCommentTopicNotificationsSerializer
			dtcns.BuildFromResource(&dtcn, db, currentUserId)
			discussionCommentTopicNotificationsSerializers = append(discussionCommentTopicNotificationsSerializers, dtcns)
		}

		w.Header().Set(
			"Content-Range",
			fmt.Sprintf("%d-%d/%d", offset, limit, totalRecords),
		)
		w.Header().Set("Range-unit", "items")
		r.JSON(w, http.StatusPartialContent, discussionCommentTopicNotificationsSerializers)
	}
}

// ShowDiscussionCommentTopicNotificationsHandler display an discussion comment topic notification
// and trigger opened on notification
func ShowDiscussionCommentTopicNotificationsHandler(w http.ResponseWriter, req *http.Request) {
	var discussion models.Discussion
	var discussionComment models.DiscussionComment
	var discussionCommentTopic models.DiscussionCommentTopic
	var discussionCommentTopicNotification models.DiscussionCommentTopicNotification
	var discussionCommentTopicNotificationsSerializer serializers.DiscussionCommentTopicNotificationsSerializer

	r := render.New()
	db := middlewares.GetDb(req)
	currentUserId := currentUserID(req)
	vars := mux.Vars(req)
	id := vars["id"]

	db.Model(&models.DiscussionCommentTopicNotification{}).Find(&discussionCommentTopicNotification, id)
	db.Model(&discussionCommentTopicNotification).Related(&discussionCommentTopic)
	db.Model(&discussionCommentTopic).Related(&discussionComment)
	db.Model(&discussionComment).Related(&discussion)

	if currentUserId != discussion.UserID {
		erHJSON(r, w, "Não foi possivel encontrar a notificação, tente novamente mais tarde", http.StatusUnauthorized)
		return
	}

	discussionCommentTopicNotification.Opened = true
	db.Save(&discussionCommentTopicNotification)

	discussionCommentTopicNotificationsSerializer.BuildFromResource(&discussionCommentTopicNotification, db, currentUserId)

	r.JSON(w, http.StatusOK, discussionCommentTopicNotificationsSerializer)
}

// TotalNotificationsUnread return a total of unread notifications
func TotalNotificationsUnread(w http.ResponseWriter, req *http.Request) {
	var discussionIDs []int
	var discussionCommentIDs []int
	var discussionTopicIDs []int
	var discussionCommentTopicIDs []int
	var totalDiscussionNotifications int
	var totalDiscussionCommentNotifications int

	r := render.New()
	db := middlewares.GetDb(req)
	currentUserId := currentUserID(req)

	db.Model(models.Discussion{}).Where("user_id = ?", currentUserId).Pluck("id", &discussionIDs)
	db.Model(models.DiscussionComment{}).Where("discussion_id in (?)", discussionIDs).Pluck("id", &discussionCommentIDs)
	db.Model(models.DiscussionTopic{}).Where("discussion_id in (?)", discussionIDs).Pluck("id", &discussionTopicIDs)
	db.Model(models.DiscussionCommentTopic{}).Where("discussion_comment_id in (?)", discussionCommentIDs).Pluck("id", &discussionCommentTopicIDs)

	db.Model(models.DiscussionTopicNotification{}).Where("discussion_topic_id in (?) and opened is false", discussionTopicIDs).Count(&totalDiscussionNotifications)
	db.Model(models.DiscussionCommentTopicNotification{}).Where("discussion_comment_topic_id in (?) and opened is false", discussionCommentTopicIDs).Count(&totalDiscussionCommentNotifications)

	r.JSON(w, http.StatusOK, map[string]int{"total": (totalDiscussionNotifications + totalDiscussionCommentNotifications)})
}
