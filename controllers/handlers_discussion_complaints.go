package controllers

import (
	"net/http"

	"github.com/freehaha/token-auth"
	"github.com/gorilla/mux"
	"github.com/nucleo-digital/plataforma-brasil-api/middlewares"
	"github.com/nucleo-digital/plataforma-brasil-api/models"
	"github.com/unrolled/render"
)

// LikeCommentDiscussionHandler handles with complaints on an discussion
func CreateComplaintDiscussionHandler(w http.ResponseWriter, req *http.Request) {
	var discussion models.Discussion
	var dc models.DiscussionComplaint

	r := render.New()
	db := middlewares.GetDb(req)
	vars := mux.Vars(req)
	id := vars["id"]

	findDiscussionQ := db.Find(&discussion, "id = ?", atoi(id))
	if findDiscussionQ.Error != nil {
		erHJSON(r, w, "Não foi possivel encontrar a discussão", http.StatusNotFound)
		return
	}

	t := tauth.Get(req)
	UserID := t.Claims("id").(float64)
	findDComplaintQ := db.Find(
		&dc,
		"discussion_id = ? AND user_id = ?",
		id, UserID,
	)

	if findDComplaintQ.Error != nil {
		dc.DiscussionID = atoi(id)
		dc.UserID = int(UserID)
		dc.IPMask = req.RemoteAddr
		db.Save(&dc)

		r.JSON(w, http.StatusCreated, map[string]string{"msg": "flaged"})
	} else {
		erHJSON(r, w, "Você já deu denunciou essa discussão", http.StatusBadRequest)
		return
	}
}

// CreateComplaintDiscussionCommentHandler handles with complaints on an discussion
func CreateComplaintDiscussionCommentHandler(w http.ResponseWriter, req *http.Request) {
	var discussion models.Discussion
	var dcm models.DiscussionComment
	var dcc models.DiscussionCommentComplaint

	r := render.New()
	db := middlewares.GetDb(req)
	vars := mux.Vars(req)
	id := vars["id"]
	commentID := vars["comment_id"]

	findDiscussionQ := db.Find(&discussion, "id = ?", id)
	if findDiscussionQ.Error != nil {
		erHJSON(r, w, "Não foi possivel encontrar a discussão", http.StatusNotFound)
		return
	}

	findCommentQ := db.Find(&dcm, "id = ?", commentID)
	if findCommentQ.Error != nil {
		erHJSON(r, w, "Não foi possivel encontrar o comentario", http.StatusNotFound)
		return
	}

	t := tauth.Get(req)
	UserID := t.Claims("id").(float64)
	findDComplaintQ := db.Find(
		&dcc,
		"discussion_comment_id = ? AND user_id = ?",
		dcm.ID, UserID,
	)

	if findDComplaintQ.Error != nil {
		dcc.DiscussionCommentID = dcm.ID
		dcc.UserID = int(UserID)
		dcc.IPMask = req.RemoteAddr
		db.Save(&dcc)

		r.JSON(w, http.StatusCreated, map[string]string{"msg": "flaged"})
	} else {
		erHJSON(r, w, "Você já deu denunciou esse comentario", http.StatusBadRequest)
		return
	}
}
