# Plataforma Brasil API

[ ![Codeship Status for nucleo-digital/plataforma-brasil-api](https://codeship.com/projects/c59e31a0-e7b2-0132-6193-2a723c8c6817/status?branch=master)](https://codeship.com/projects/82615)

## Requerimentos

* Go - ```brew install golang``` or ```apt-get install golang```
* GoDep - ```go get github.com/tools/godep```
* fresh - ```go get github.com/pilu/fresh```

## Como instalar

* ```mkdir ~/go/src/github.com/nucleo-digital && cd ~/go/src/github.com/nucleo-digital```
* ```git clone git@github.com:nucleo-digital/plataforma-brasil-api.git```
* ```cd plataforma-brasil-api```
* ```godep restore```
* ```cp env.local .env``
* ```fresh```

## Schema do Banco de Dados

* Com a api funcionando, rode o comando ```/setupdb``` para criar a estrutura base da plataforma
* Após execute as sqls que estão no arquivo ```views.sql```
* É necessário configurar o site a partir do Administrador, com atenção especial para os Temas, Páginas e Configurações.
* Para testar a primeira fase é necessário instalar e configurar a api do pairwise;

## Servidor

Você pode encontrar mais detalhes na [wiki](https://github.com/nucleo-digital/plataforma-brasil-api/wiki/Configura%C3%A7%C3%A3o-do-Servidor).

## API

* [Blueprint specification](https://github.com/nucleo-digital/plataforma-brasil-api/blob/develop/docs/blueprint.md)
* [Apiary docs](https://app.apiary.io/platbr/)
