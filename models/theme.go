package models

import (
	"github.com/avelino/slugify"
	"github.com/mholt/binding"
	"time"
)

type ThemeForm struct {
	Theme struct {
		ID         int        `json:"id" gorm:"primary_key"`
		Name       string     `json:"name" sql:"not null"`
		Position   int        `json:"position" sql:"not null"`
		Active     bool       `json:"active" sql:"not null;default:false"`
		Categories []Category `json:"categories" gorm:"many2many:category_themes;"`
		CreatedAt  time.Time  `json:"created_at"`
	}
}

// FieldMap maps the request fields to a theme model
func (th *ThemeForm) FieldMap() binding.FieldMap {
	return binding.FieldMap{
		&th.Theme.Name: binding.Field{
			Form:     "name",
			Required: true,
		},
		&th.Theme.Position: binding.Field{
			Form:     "position",
			Required: true,
		},
		&th.Theme.Active: binding.Field{
			Form: "active",
			//Required: true,
		},
		&th.Theme.Categories: binding.Field{
			Form:     "categories",
			Required: true,
		},
	}
}

// BeforeSave category we set the slug
func (ct *Category) BeforeSave() (err error) {
	if ct.Slug == "" {
		ct.Slug = slugify.Slugify(ct.Name)
	}

	return nil
}

// FieldMap maps the request fields to a category model
func (ct *Category) FieldMap() binding.FieldMap {
	return binding.FieldMap{
		&ct.Name: binding.Field{
			Form:     "name",
			Required: true,
		},
	}
}
