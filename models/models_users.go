package models

import (
	"net/http"

	"github.com/mholt/binding"
)

type UserInvite struct {
	Email   string `json:"email"`
	Message string `json:"message"`
}

func (lf *UserInvite) FieldMap() binding.FieldMap {
	return binding.FieldMap{
		&lf.Email: binding.Field{
			Form:     "email",
			Required: true,
		},
		&lf.Message: binding.Field{
			Form:     "message",
			Required: true,
		},
	}
}

type ResetPassword struct {
	ResetCode string `json:"reset_code"`
	Password  string `json:"password"`
}

func (lf *ResetPassword) FieldMap() binding.FieldMap {
	return binding.FieldMap{
		&lf.ResetCode: binding.Field{
			Form:     "reset_code",
			Required: true,
		},
		&lf.Password: binding.Field{
			Form:     "password",
			Required: true,
		},
	}
}

type Login struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (lf *Login) FieldMap() binding.FieldMap {
	return binding.FieldMap{
		&lf.Email: binding.Field{
			Form:     "email",
			Required: true,
		},
		&lf.Password: binding.Field{
			Form:     "password",
			Required: true,
		},
	}
}

func (uf *User) FieldMap() binding.FieldMap {
	return binding.FieldMap{
		&uf.Nome: binding.Field{
			Form:     "nome",
			Required: true,
		},
		&uf.Email: binding.Field{
			Form:     "email",
			Required: true,
		},
		&uf.PhotoUrl:       "photo_url",
		&uf.Password:       "password",
		&uf.DataNascimento: "data_nascimento",
		&uf.Ocupacao:       "ocupacao",
		&uf.Cidade:         "cidade",
		&uf.Estado:         "estado",
		&uf.Cpf:            "cpf",
		&uf.Descricao:      "descricao",
		&uf.Genero:         "genero",
		&uf.FacebookID:     "facebook_id",
	}
}

func (uf User) Validate(req *http.Request, errs binding.Errors) binding.Errors {
	if uf.FacebookID == "" && uf.Cpf == "" {
		errs = append(errs, binding.Error{
			FieldNames:     []string{"cpf"},
			Classification: "ComplaintError",
			Message:        "When user not login with facebook, cpf is required.",
		})
	}

	if !emailRegex.MatchString(uf.Email) {
		errs = append(errs, binding.Error{
			FieldNames:     []string{"email"},
			Classification: "ComplaintError",
			Message:        "Email is invalid.",
		})
	}
	return errs
}

func (pv *SiteConfiguration) FieldMap() binding.FieldMap {
	return binding.FieldMap{
		&pv.Key:   "key",
		&pv.Value: "value",
	}
}
func (pv *Configuration) FieldMap() binding.FieldMap {
	return binding.FieldMap{
		&pv.Key:   "key",
		&pv.Value: "value",
	}
}
