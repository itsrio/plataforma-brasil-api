package models

import (
	"fmt"
	"net/mail"

	"github.com/jinzhu/gorm"
	"github.com/mholt/binding"
	"github.com/nucleo-digital/plataforma-brasil-api/services"
)

//	ID          int        `json:"id" gorm:"primary_key"`
//	Title       string     `json:"title" sql:"not null"`
//	Description string     `json:"description" sql:"not null"`
//	Categories  []Category `json:"categories" gorm:"many2many:category_discussions;"`
//	Theme       Theme      `json:"theme"`

// FieldMap maps the request fields to a phase model
func (dis *Discussion) FieldMap() binding.FieldMap {
	return binding.FieldMap{
		&dis.Description: binding.Field{
			Form:     "description",
			Required: true,
		},
		&dis.Categories: binding.Field{
			Form:     "categories",
			Required: true,
		},
		&dis.ThemeID: binding.Field{
			Form:     "theme_id",
			Required: true,
		},
	}
}

// AfterCreate discussion callback to handles with discussion topic creation
//func (dis *Discussion) AfterCreate(tx *gorm.DB) (err error) {
//	dis.CreateTopic(tx)
//	return
//}

// CreateTopic create discussion topic
func (dis *Discussion) CreateTopic(tx *gorm.DB) (err error) {
	topic := DiscussionTopic{
		DiscussionID: dis.ID,
	}
	tx.Create(&topic)
	return
}

// FieldMap maps the request fields to a phase model
func (dc *DiscussionComment) FieldMap() binding.FieldMap {
	return binding.FieldMap{
		&dc.Description: binding.Field{
			Form:     "description",
			Required: true,
		},
	}
}

// CreateDiscussionLikeNotification create discussion like notification
func (dl *DiscussionLike) CreateDiscussionLikeNotification(tx *gorm.DB) (err error) {
	var discussion Discussion
	var discussionTopic DiscussionTopic

	tx.Model(&dl).Related(&discussion)
	tx.Find(&discussionTopic, "discussion_id = ?", discussion.ID)

	discussionTopicNotification := DiscussionTopicNotification{
		DiscussionTopicID: discussionTopic.ID,
		TemplateName:      "new_discussion_like",
		DiscussionLikeID:  dl.ID,
	}
	tx.Create(&discussionTopicNotification)
	discussionTopicNotification.DeliverEmailNotifications(tx)

	return
}

//CreateDiscussionDislikeNotification create discussion dislike notification
func (dl *DiscussionDislike) CreateDiscussionDislikeNotification(tx *gorm.DB) (err error) {
	var discussion Discussion
	var discussionTopic DiscussionTopic

	tx.Model(&dl).Related(&discussion)
	tx.Find(&discussionTopic, "discussion_id = ?", discussion.ID)

	discussionTopicNotification := DiscussionTopicNotification{
		DiscussionTopicID:   discussionTopic.ID,
		TemplateName:        "new_discussion_dislike",
		DiscussionDislikeID: dl.ID,
	}
	tx.Create(&discussionTopicNotification)
	discussionTopicNotification.DeliverEmailNotifications(tx)

	return
}

// CreateDiscussionCommentNotification create a comment discussion like notification
func (dl *DiscussionCommentLike) CreateDiscussionCommentNotification(tx *gorm.DB) (err error) {
	var discussionCommentTopic DiscussionCommentTopic
	var discussionComment DiscussionComment

	tx.Model(&dl).Related(&discussionComment)
	tx.Find(&discussionCommentTopic, "discussion_comment_id = ?", discussionComment.ID)

	discussionCommentTopicNotification := DiscussionCommentTopicNotification{
		TemplateName:             "new_discussion_comment_like",
		DiscussionCommentLikeID:  dl.ID,
		DiscussionCommentTopicID: discussionCommentTopic.ID,
	}
	tx.Create(&discussionCommentTopicNotification)
	discussionCommentTopicNotification.DeliverEmailNotifications(tx)

	return
}

// CreateDiscussionCommentNotification create a comment discussion dislike notification
func (dl *DiscussionCommentDisLike) CreateDiscussionCommentNotification(tx *gorm.DB) (err error) {
	var discussionCommentTopic DiscussionCommentTopic
	var discussionComment DiscussionComment

	tx.Model(&dl).Related(&discussionComment)
	tx.Find(&discussionCommentTopic, "discussion_comment_id = ?", discussionComment.ID)

	discussionCommentTopicNotification := DiscussionCommentTopicNotification{
		TemplateName:               "new_discussion_comment_dislike",
		DiscussionCommentDisLikeID: dl.ID,
		DiscussionCommentTopicID:   discussionCommentTopic.ID,
	}
	tx.Create(&discussionCommentTopicNotification)
	discussionCommentTopicNotification.DeliverEmailNotifications(tx)

	return
}

// CreateTopic handles with topic creation and deliver
// defaults notifications when new comment as made
func (dc *DiscussionComment) CreateTopic(tx *gorm.DB) (err error) {
	var discussionTopic DiscussionTopic

	topic := DiscussionCommentTopic{DiscussionCommentID: dc.ID}
	tx.Create(&topic)

	tx.Find(&discussionTopic, "discussion_id = ?", dc.DiscussionID)

	discussionTopicNotification := DiscussionTopicNotification{
		TemplateName:             "new_comment",
		DiscussionCommentTopicID: topic.ID,
		DiscussionTopicID:        discussionTopic.ID,
	}
	tx.Create(&discussionTopicNotification)
	discussionTopicNotification.DeliverEmailNotifications(tx)

	return
}

// FindDiscussion should return a related discussion on discussion topic notification
func (dn *DiscussionTopicNotification) FindDiscussion(tx *gorm.DB) Discussion {
	var discussionTopic DiscussionTopic
	var discussion Discussion

	tx.Model(&dn).Related(&discussionTopic)
	tx.Model(&discussionTopic).Related(&discussion)

	return discussion
}

// IsNewCommentEvent check if notification
// is from a new comment made on dicussion and returns true or false
func (dn *DiscussionTopicNotification) IsNewCommentEvent() bool {
	return dn.TemplateName == "new_comment"
}

// IsLikeEvent check if notification
// is from a like event made on dicussion and returns true or false
func (dn *DiscussionTopicNotification) IsLikeEvent() bool {
	return dn.TemplateName == "new_discussion_like"
}

// IsDislikeEvent check if notification
// is from a dislike event made on dicussion and returns true or false
func (dn *DiscussionTopicNotification) IsDislikeEvent() bool {
	return dn.TemplateName == "new_discussion_dislike"
}

// NewCommentNotify sends a notification email about
// new comment on discussion event
func (dn *DiscussionTopicNotification) NewCommentNotify(tx *gorm.DB) error {
	var fromUser User
	var toUser User
	var discussion Discussion
	var discussionComment DiscussionComment

	tx.Model(&discussionComment).
		Joins(`
			inner join discussion_comment_topics dct
				on dct.id = dct.discussion_comment_id`).
		Where(`
			dct.id = ?`, dn.DiscussionCommentTopicID).
		Find(&discussionComment)

	tx.Model(&discussionComment).Related(&fromUser)
	tx.Model(&discussionComment).Related(&discussion)
	tx.Model(&discussion).Related(&toUser)

	messageTemplate := fmt.Sprintf(`
		%s  adicionou um novo comentário na discussão "%s"
		`, fromUser.Nome, discussion.Title)

	return services.SendTextEmail(
		"Novo comentário em sua discussão",
		messageTemplate,
		mail.Address{
			Name:    fromUser.Nome,
			Address: fromUser.Email,
		},
		mail.Address{
			Name:    toUser.Nome,
			Address: toUser.Email,
		})
}

// LikeNotify sends a notification email about
// a new like event on discussion
func (dn *DiscussionTopicNotification) LikeNotify(tx *gorm.DB) error {
	var fromUser User
	var toUser User
	var discussion Discussion
	var discussionLike DiscussionLike

	tx.Model(&dn).Related(&discussionLike)
	tx.Model(&discussionLike).Related(&fromUser)
	tx.Model(&discussionLike).Related(&discussion)
	tx.Model(&discussion).Related(&toUser)

	messageTemplate := fmt.Sprintf(`
	%s  gostou da discussão "%s"
	`, fromUser.Nome, discussion.Title)

	return services.SendTextEmail(
		"Alguem gostou de sua discussão",
		messageTemplate,
		mail.Address{
			Name:    fromUser.Nome,
			Address: fromUser.Email,
		},
		mail.Address{
			Name:    toUser.Nome,
			Address: toUser.Email,
		})
}

// DisLikeNotify sends a notification email about
// a new dislike event on discussion
func (dn *DiscussionTopicNotification) DisLikeNotify(tx *gorm.DB) error {
	var fromUser User
	var toUser User
	var discussion Discussion
	var discussionDisLike DiscussionDislike

	tx.Model(&dn).Related(&discussionDisLike)
	tx.Model(&discussionDisLike).Related(&fromUser)
	tx.Model(&discussionDisLike).Related(&discussion)
	tx.Model(&discussion).Related(&toUser)

	messageTemplate := fmt.Sprintf(`
	%s  não gostou da discussão "%s"
	`, fromUser.Nome, discussion.Title)

	return services.SendTextEmail(
		"Alguem não gostou de sua discussão",
		messageTemplate,
		mail.Address{
			Name:    fromUser.Nome,
			Address: fromUser.Email,
		},
		mail.Address{
			Name:    toUser.Nome,
			Address: toUser.Email,
		})
}

// DeliverEmailNotifications send emails from
// created discussion topic notifications
func (dn *DiscussionTopicNotification) DeliverEmailNotifications(tx *gorm.DB) (err error) {
	switch {
	case dn.IsNewCommentEvent():
		return dn.NewCommentNotify(tx)
	case dn.IsLikeEvent():
		return dn.LikeNotify(tx)
	case dn.IsDislikeEvent():
		return dn.DisLikeNotify(tx)
	}

	return nil
}

// IsLikeEvent check if notification
// is from a like event made on dicussion comment and returns true or false
func (dn *DiscussionCommentTopicNotification) IsLikeEvent() bool {
	return dn.TemplateName == "new_discussion_comment_like"
}

// IsDislikeEvent check if notification
// is from a dislike event made on dicussion comment and returns true or false
func (dn *DiscussionCommentTopicNotification) IsDislikeEvent() bool {
	return dn.TemplateName == "new_discussion_comment_dislike"
}

// LikeNotify sends a notification email about
// a new like event on discussion comment
func (dn *DiscussionCommentTopicNotification) LikeNotify(tx *gorm.DB) error {
	var fromUser User
	var toUser User
	//var discussion Discussion
	var discussionComment DiscussionComment
	var discussionCommentLike DiscussionCommentLike

	tx.Model(&dn).Related(&discussionCommentLike)
	tx.Model(&discussionCommentLike).Related(&fromUser)
	tx.Model(&discussionCommentLike).Related(&discussionComment)
	//tx.Model(&discussionComment).Related(&discussion)
	tx.Model(&discussionComment).Related(&toUser)

	messageTemplate := fmt.Sprintf(`
	%s gostou do seu comentario"%s"
	`, fromUser.Nome, discussionComment.Description)

	return services.SendTextEmail(
		"Alguem gostou do seu comentario",
		messageTemplate,
		mail.Address{
			Name:    fromUser.Nome,
			Address: fromUser.Email,
		},
		mail.Address{
			Name:    toUser.Nome,
			Address: toUser.Email,
		})
}

// DisLikeNotify sends a notification email about
// a new like event on discussion comment
func (dn *DiscussionCommentTopicNotification) DisLikeNotify(tx *gorm.DB) error {
	var fromUser User
	var toUser User
	//var discussion Discussion
	var discussionComment DiscussionComment
	var discussionCommentDisLike DiscussionCommentDisLike

	tx.Model(&dn).Related(&discussionCommentDisLike)
	tx.Model(&discussionCommentDisLike).Related(&fromUser)
	tx.Model(&discussionCommentDisLike).Related(&discussionComment)
	//tx.Model(&discussionComment).Related(&discussion)
	tx.Model(&discussionComment).Related(&toUser)

	messageTemplate := fmt.Sprintf(`
	%s não gostou do seu comentario"%s"
	`, fromUser.Nome, discussionComment.Description)

	return services.SendTextEmail(
		"Alguem não gostou do seu comentario",
		messageTemplate,
		mail.Address{
			Name:    fromUser.Nome,
			Address: fromUser.Email,
		},
		mail.Address{
			Name:    toUser.Nome,
			Address: toUser.Email,
		})
}

// DeliverEmailNotifications send emails from
// created discussion comment topic notifications
func (dn *DiscussionCommentTopicNotification) DeliverEmailNotifications(tx *gorm.DB) (err error) {
	switch {
	case dn.IsLikeEvent():
		return dn.LikeNotify(tx)
	case dn.IsDislikeEvent():
		return dn.DisLikeNotify(tx)
	}

	return nil
}

// DiscussionsByThemeId filter discussion by themes
func DiscussionsByThemeId(themeID int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if themeID > 0 {
			return db.Where("discussions.theme_id = ?", themeID)
		}

		return db
	}
}

// DiscussionsDefaultJoins handles with default joins
func DiscussionsDefaultJoins() func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		return db.Joins(`
			left join discussion_scores ds on ds.discussion_id = discussions.id
			left join category_discussions cd on cd.discussion_id = discussions.id
			inner join users author on author.id = discussions.user_id
			inner join categories cat on cat.id = cd.category_id
			left outer join discussion_complaints dc on dc.discussion_id = discussions.id
		`)
	}
}

// HideOrShowDiscussionsWhereFlagedFor show only all flaged discussion when flag param is "true"
// or hide only the flagged dicussions for given userId on parameter
func HideOrShowDiscussionsWhereFlagedFor(flagParam string, userID int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if flagParam == "true" {
			return db.Where("dc.id is not null")
		}
		if userID > 0 {
			return db.Where("dc.user_id <> ? or dc.user_id is null", userID)
		}
		return db
	}
}

// HideOrShowCommentsFor get only comments that user dont flaged
func HideOrShowCommentsFor(flagParam string, userID int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		db = db.Joins(`
			left outer join discussion_comment_complaints dcc
				on dcc.discussion_comment_id = discussion_comments.id
		`)

		if flagParam == "true" {
			return db.Select("DISTINCT discussion_comments.*").Where("dcc.id is not null")
		}

		if userID > 0 {
			return db.Select("DISTINCT discussion_comments.*").Where("dcc.user_id <> ? or dcc.user_id is null", userID)
		}

		return db
	}
}

// DiscussionsWhereAuthorMatches filter discussions by author name
func DiscussionsWhereAuthorMatches(authorString string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if authorString != "" {
			var discussionsIdsFromComments []int
			db.Select("DISTINCT discussions.*").
				Model(&Discussion{}).
				Joins(`
					left join discussion_scores ds on ds.discussion_id = discussions.id
					left join category_discussions cd on cd.discussion_id = discussions.id
					inner join users author on author.id = discussions.user_id
					inner join categories cat on cat.id = cd.category_id
					left outer join discussion_complaints dc on dc.discussion_id = discussions.id
					left join discussion_comments dcm on dcm.discussion_id = discussions.id
					join users u on u.id =dcm.user_id`).
				Where("u.nome REGEXP ?", authorString).
				Pluck("discussions.id", &discussionsIdsFromComments)

			if len(discussionsIdsFromComments) > 0 {
				return db.Where("author.nome REGEXP ? OR discussions.id IN(?)", authorString, discussionsIdsFromComments)
			}

			return db.Where("author.nome REGEXP ?", authorString)
			//maybe in future ;)
			//return db.Where("match(author.nome) against (? IN NATURAL LANGUAGE MODE)", authorString)
		}

		return db
	}
}

// DiscussionsByCategory filter discussions that have the given category
func DiscussionsByCategory(categorySlug string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if categorySlug != "" {
			return db.Where("cat.slug = ?", categorySlug)
		}
		return db
	}
}

// DiscussionsByOrder reorder by given order param
func DiscussionsByOrder(order string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		var orderString string

		switch order {
		case "recent":
			orderString = "discussions.created_at desc, ds.score desc"
		case "oldest":
			orderString = "discussions.created_at asc"
		case "coldest":
			orderString = "ds.score asc"
		default:
			orderString = "ds.score desc, discussions.created_at desc"
		}

		return db.Order(orderString)
	}
}
