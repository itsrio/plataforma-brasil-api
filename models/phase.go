package models

import "github.com/mholt/binding"

// FieldMap maps the request fields to a phase model
func (ph *Phase) FieldMap() binding.FieldMap {
	return binding.FieldMap{
		&ph.Num: binding.Field{
			Form:     "phase_num",
			Required: true,
		},
		&ph.WhenFinish: binding.Field{
			Form:     "when_finish",
			Required: true,
		},
	}
}
