package models

import (
	"github.com/mholt/binding"
)

type ParamsSkip struct {
	Skip struct {
		VisitorIdentifier string `json:"visitor_identifier"`
		SkipReason        string `json:"skip_reason"`
		AppearanceLookup  string `json:"appearance_lookup"`
		TimeViewed        string `json:"time_viewed"`
		ForceInvalidVote  string `json:"force_invalid_vote"`
	} `json:"skip"`
	NextPrompt struct {
		VisitorIdentifier string `json:"visitor_identifier"`
		WithAppearance    string `json:"with_appearance"`
		Algorithm         string `json:"algorithm"`
		WithVisitorStats  string `json:"with_visitor_stats"`
	} `json:"next_prompt"`
}

type QueryStringQuestionShow struct {
	VisitorIdentifier string `url:"visitor_identifier"`
	WithPrompt        string `url:"with_prompt"`
	WithAppearance    string `url:"with_appearance"`
	WithVisitorStats  string `url:"with_visitor_stats"`
	Algorithm         string `url:"algorithm"`
	Version           string `url:"version"`
}

type QueryStringPromptVote struct {
	VisitorIdentifier  string `url:"vote[visitor_identifier]"`
	Direction          string `url:"vote[direction]"`
	AppearanceLookup   string `url:"vote[appearance_lookup]"`
	TimeViewed         string `url:"vote[time_viewed]"`
	ForceInvalidVote   string `url:"vote[force_invalid_vote]"`
	Tracking           string `url:"vote[tracking]"`
	NVisitorIdentifier string `url:"next_prompt[visitor_identifier]"`
	NWithAppearance    string `url:"next_prompt[with_appearance]"`
	NAlgorithm         string `url:"next_prompt[algorithm]"`
	NWithVisitorStats  string `url:"next_prompt[with_visitor_stats]"`
}

type ParamsPromptVote struct {
	Vote struct {
		VisitorIdentifier string `json:"visitor_identifier, omitempty"`
		Direction         string `json:"direction, omitempty"`
		AppearanceLookup  string `json:"appearance_lookup, omitempty"`
		TimeViewed        string `json:"time_viewed, omitempty"`
		ForceInvalidVote  string `json:"force_invalid_vote, omitempty"`
		Tracking          string `json:"tracking, omitempty"`
		ChoiceText        string `json:"choice_text"`
		ChoiceID          string `json:"choice_id"`
	} `json:"vote"`
	NextPrompt struct {
		VisitorIdentifier string `json:"visitor_identifier, omitempty"`
		WithAppearance    string `json:"with_appearance, omitempty"`
		Algorithm         string `json:"algorithm, omitempty"`
		WithVisitorStats  string `json:"with_visitor_stats, omitempty"`
	} `json:"next_prompt"`
}

func (pv *ParamsPromptVote) FieldMap() binding.FieldMap {
	return binding.FieldMap{
		&pv.Vote.VisitorIdentifier: "visitor_identifier",
		&pv.Vote.Direction:         "direction",
		&pv.Vote.AppearanceLookup:  "appearance_lookup",
		&pv.Vote.TimeViewed:        "time_viewed",
		&pv.Vote.ForceInvalidVote:  "force_invalid_vote",
		&pv.Vote.Tracking:          "tracking",

		&pv.NextPrompt.VisitorIdentifier: "visitor_identifier",
		&pv.NextPrompt.WithAppearance:    "appearance_lookup",
		&pv.NextPrompt.Algorithm:         "algorithm",
		&pv.NextPrompt.WithVisitorStats:  "with_visitor_stats",
	}
}

type ParamsChoiceCreate struct {
	VisitorIdentifier string `json:"visitor_identifier"`
	Data              string `json:"data"`
	Active            bool   `json:"active"`
	LocalIdentifier   string `json:"local_identifier"`
}

func (pv *ParamsChoiceCreate) FieldMap() binding.FieldMap {
	return binding.FieldMap{
		&pv.VisitorIdentifier: "visitor_identifier",
		&pv.Data:              "data",
		&pv.Active:            "active",
		&pv.LocalIdentifier:   "local_identifier",
	}
}

type AdminParamsChoiceCreate struct {
	ParamsChoiceCreate `json:"choice"`
}

type Question struct {
	Active                    string `json:"active"`
	ActiveItemsCount          string `json:"active-items-count"`
	ChoicesCount              string `json:"choices-count"`
	CreatedAt                 string `json:"created-at"`
	CreatorId                 string `json:"creator-id"`
	Id                        string `json:"id"`
	InactiveChoicesCount      string `json:"inactive-choices-count"`
	ItShouldAutoactivateIdeas string `json:"it-should-autoactivate-ideas"`
	ItemsCount                string `json:"items-count"`
	LocalIdentifier           string `json:"local-identifier"`
	Name                      string `json:"name"`
	PromptsCount              string `json:"prompts-count"`
	SiteId                    string `json:"site-id"`
	UpdatedAt                 string `json:"updated-at"`
	UsesCatchup               string `json:"uses-catchup"`
	VotesCount                string `json:"votes-count"`
	ItemCount                 string `json:"item-count"`
	VisitorVotes              string `json:"visitor_votes"`
	VisitorIdeas              string `json:"visitor_ideas"`
}

type PromptShow struct {
	QuestionName    string `json:"question-name"`
	CreatedAt       string `json:"created-at"`
	Id              string `json:"prompt-id"`
	LeftChoiceId    string `json:"left-choice-id"`
	QuestionId      string `json:"question-id"`
	RightChoiceId   string `json:"right-choice-id"`
	UpdatedAt       string `json:"updated-at"`
	VotesCount      string `json:"votes-count"`
	LeftChoiceText  string `json:"left-choice-text"`
	RightChoiceText string `json:"right-choice-text"`
}

type PromptVoteOrSkip struct {
	CreatedAt       string `json:"created-at"`
	PromptId        string `json:"prompt-id"`
	QuestionId      string `json:"question-id"`
	UpdatedAt       string `json:"updated-at"`
	VotesCount      string `json:"votes-count"`
	LeftChoiceText  string `json:"left-choice-text"`
	RightChoiceText string `json:"right-choice-text"`
	LeftChoiceId    string `json:"left-choice-id"`
	RightChoiceId   string `json:"right-choice-id"`
}

type ChoiceCreate struct {
	Active                 string `json: "active"`
	CreatedAt              string `json: "created-at"`
	CreatorId              string `json: "creator-id"`
	Data                   string `json: "data"`
	Id                     string `json: "id"`
	ItemId                 string `json: "item-id"`
	LocalIdentifier        string `json: "local-identifier"`
	LossCount              string `json: "loss-count"`
	Losses                 string `json: "losses"`
	Position               string `json: "position"`
	PromptId               string `json: "prompt-id"`
	PromptsCount           string `json: "prompts-count"`
	PromptsOnTheLeftCount  string `json: "prompts-on-the-left-count"`
	PromptsOnTheRightCount string `json: "prompts-on-the-right-count"`
	QuestionId             string `json: "question-id"`
	Ratings                string `json: "ratings"`
	RequestId              string `json: "request-id"`
	Score                  string `json: "score"`
	Tracking               string `json: "tracking"`
	UpdatedAt              string `json: "updated-at"`
	VotesCount             string `json: "votes-count"`
	Wins                   string `json: "wins"`
}

type Choices struct {
	Choice []struct {
		Active      string `json:"active"`
		CreatedAt   string `json:"created-at"`
		Data        string `json:"data"`
		Id          string `json:"id"`
		Losses      string `json:"losses"`
		Score       string `json:"score"`
		Wins        string `json:"wins"`
		UserCreated string `json:"user-created"`
	} `json:"choices"`
}

type MyResults struct {
	Votes []struct {
		Winner          string `json:"winner"`
		Id              int    `json:"id"`
		LeftChoiceId    int    `json:"left_choice_id"`
		LeftChoiceData  string `json:"left_choice_data"`
		RightChoiceId   int    `json:"right_choice_id"`
		RightChoiceData string `json:"right_choice_data"`
	} `json:"votes"`
}

type MySummary struct {
	Votes       string `json:"votes"`
	Skips       string `json:"skips"`
	Suggestions string `json:"suggestions"`
	Invites     string `json:"invites"`
}

type FilteredResult struct {
	ChoiceID   string `json:"choice_id"`
	ChoiceText string `json:"choice_text"`
	Total      string `json:"total"`
}
