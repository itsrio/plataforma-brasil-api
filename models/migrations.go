package models

import (
	"regexp"
	"time"
)

const (
	alphaRegexString        = "^[a-zA-Z]+$"
	alphaNumericRegexString = "^[a-zA-Z0-9]+$"
	numericRegexString      = "^[-+]?[0-9]+(?:\\.[0-9]+)?$"
	numberRegexString       = "^[0-9]+$"
	hexadecimalRegexString  = "^[0-9a-fA-F]+$"
	hexcolorRegexString     = "^#(?:[0-9a-fA-F]{3}|[0-9a-fA-F]{6})$"
	rgbRegexString          = "^rgb\\(\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])\\s*,\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])\\s*,\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])\\s*\\)$"
	rgbaRegexString         = "^rgba\\(\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])\\s*,\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])\\s*,\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])\\s*,\\s*((0.[1-9]*)|[01])\\s*\\)$"
	hslRegexString          = "^hsl\\(\\s*(0|[1-9]\\d?|[12]\\d\\d|3[0-5]\\d|360)\\s*,\\s*((0|[1-9]\\d?|100)%)\\s*,\\s*((0|[1-9]\\d?|100)%)\\s*\\)$"
	hslaRegexString         = "^hsla\\(\\s*(0|[1-9]\\d?|[12]\\d\\d|3[0-5]\\d|360)\\s*,\\s*((0|[1-9]\\d?|100)%)\\s*,\\s*((0|[1-9]\\d?|100)%)\\s*,\\s*((0.[1-9]*)|[01])\\s*\\)$"
	emailRegexString        = "^(((([a-zA-Z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])+(\\.([a-zA-Z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])+)*)|((\\x22)((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])|(\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-zA-Z]|\\d|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])|(([a-zA-Z]|\\d|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])([a-zA-Z]|\\d|-|\\.|_|~|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])*([a-zA-Z]|\\d|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])))\\.)+(([a-zA-Z]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])|(([a-zA-Z]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])([a-zA-Z]|\\d|-|\\.|_|~|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])*([a-zA-Z]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])))\\.?$"
)

var (
	alphaRegex        = regexp.MustCompile(alphaRegexString)
	alphaNumericRegex = regexp.MustCompile(alphaNumericRegexString)
	numericRegex      = regexp.MustCompile(numericRegexString)
	numberRegex       = regexp.MustCompile(numberRegexString)
	hexadecimalRegex  = regexp.MustCompile(hexadecimalRegexString)
	hexcolorRegex     = regexp.MustCompile(hexcolorRegexString)
	rgbRegex          = regexp.MustCompile(rgbRegexString)
	rgbaRegex         = regexp.MustCompile(rgbaRegexString)
	hslRegex          = regexp.MustCompile(hslRegexString)
	hslaRegex         = regexp.MustCompile(hslaRegexString)
	emailRegex        = regexp.MustCompile(emailRegexString)
)

// LikeStruct just and abstractic struct for use on Like models
type LikeStruct struct {
	ID        int `gorm:"primary_key"`
	UserID    int `sql:"index"`
	IPMask    string
	CreatedAt time.Time
	UpdatedAt time.Time
}

type User struct {
	ID             int    `json:"id"`
	Nome           string `json:"nome" sql:"not null"`
	PhotoUrl       string `json:"photo_url" sql:"not null"`
	Email          string `json:"email" sql:"type:varchar(150);unique_index;not null"`
	Password       string `json:"password,omitempty" sql:"not null"`
	DataNascimento string `json:"data_nascimento"`
	Ocupacao       string `json:"ocupacao"`
	Cidade         string `json:"cidade"`
	Estado         string `json:"estado"`
	Cpf            string `json:"cpf"`
	Descricao      string `json:"descricao" sql:"type:varchar(1000);"`
	Genero         string `json:"genero"`
	FacebookID     string `json:"facebook_id"`
	Sector         string `json:"sector"`
	ResetCode      string
	CreatedAt      time.Time
	UpdatedAt      time.Time
	DeletedAt      time.Time
}

type SiteConfiguration struct {
	ID        int    `json:"id"`
	Key       string `json:"key"`
	Value     string `json:"value"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time
}
type Configuration struct {
	ID        int    `json:"id"`
	UserID    int    `json:"user_id" sql:"index"`
	Key       string `json:"key"`
	Value     string `json:"value"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time
}

type Vote struct {
	ID         int    `json:"id"`
	UserID     int    `json:"user_id" sql:"index"`
	ChoiceText string `json:"choice_text"`
	ChoiceID   int    `json:"choice_id"`
	CreatedAt  time.Time
}

type Skip struct {
	ID        int `json:"id"`
	UserID    int `json:"user_id" sql:"index"`
	PromptID  int `json:"prompt_id"`
	CreatedAt time.Time
}

type Invite struct {
	ID        int    `json:"id"`
	UserID    int    `json:"user_id" sql:"index"`
	EmailTo   string `json:"email_to" sql:"type:varchar(100)"`
	Message   string `json:"message" sql:"type:text"`
	CreatedAt time.Time
}

type Suggestion struct {
	ID         int    `json:"id"`
	UserID     int    `json:"user_id" sql:"index"`
	ChoiceText string `json:"choice_text"`
	CreatedAt  time.Time
}

type Phase struct {
	ID         int       `json:"-" gorm:"primary_key"`
	Num        int       `json:"phase" sql:"not null"`
	WhenFinish time.Time `json:"when_finish"`
}

type Theme struct {
	ID         int        `json:"id" gorm:"primary_key"`
	Name       string     `json:"name" sql:"not null"`
	Position   int        `json:"position" sql:"not null"`
	Active     bool       `json:"active" sql:"not null;default:false"`
	Categories []Category `json:"categories" gorm:"many2many:category_themes;"`
	CreatedAt  time.Time  `json:"created_at"`
}

type Category struct {
	ID        int    `json:"id" gorm:"primary_key"`
	Name      string `json:"name" sql:"not null"`
	Slug      string `json:"slug" sql:"not null"`
	CreatedAt time.Time
}

type Discussion struct {
	ID          int        `json:"id" gorm:"primary_key"`
	Title       string     `json:"title"`
	Description string     `json:"description" sql:"not null"`
	Categories  []Category `json:"categories" gorm:"many2many:category_discussions;"`
	Theme       Theme      `json:"theme"`
	ThemeID     int        `json:"theme_id" sql:"index"`
	User        User       `json:"user"`
	UserID      int        `json:"-" sql:"index"`
	Visible     bool       `json:visible sql:"not null;default:true"`
	Complaints  []DiscussionComplaint
	Comments    []DiscussionComment
	Likes       []DiscussionLike
	DisLikes    []DiscussionDislike
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

type DiscussionTopic struct {
	ID           int        `json:"id" gorm:"primary_key"`
	Discussion   Discussion `json:"discussion"`
	DiscussionID int        `json:"discussion_id" sql:"index"`
}

type DiscussionTopicNotification struct {
	ID                       int                    `json:"id" gorm:"primary_key"`
	DiscussionTopic          DiscussionTopic        `json:"discussion_topic"`
	DiscussionTopicID        int                    `json:"discussion_topic_id" sql:"index"`
	TemplateName             string                 `json:"template_name"`
	DiscussionCommentTopic   DiscussionCommentTopic `json:"discussion_comment_topic"`
	DiscussionCommentTopicID int                    `json:"discussion_comment_topic_id" sql:"index"`
	DiscussionLike           DiscussionLike         `json:"discussion_like"`
	DiscussionLikeID         int                    `json:"discussion_like_id" sql:"index"`
	DiscussionDislike        DiscussionDislike      `json:"discussion_dislike"`
	DiscussionDislikeID      int                    `json:"discussion_dislike_id" sql:"index"`
	Opened                   bool                   `json:"opened" sql:"default:false"`
	CreatedAt                time.Time
	UpdatedAt                time.Time
}

type DiscussionLike struct {
	LikeStruct
	DiscussionID int `sql:"index"`
}

type DiscussionDislike struct {
	LikeStruct
	DiscussionID int `sql:"index"`
}

type DiscussionComplaint struct {
	LikeStruct
	DiscussionID int `sql:"index"`
}

type DiscussionComment struct {
	ID           int    `json:"id" gorm:"primary_key"`
	UserID       int    `sql:"index"`
	DiscussionID int    `sql:"index"`
	Description  string `json:"description"`
	Visible      bool   `json:visible sql:"not null;default:true"`
	Likes        []DiscussionCommentLike
	DisLikes     []DiscussionCommentDisLike
	CreatedAt    time.Time
	UpdatedAt    time.Time
}

type DiscussionCommentTopic struct {
	ID                  int               `json:"id" gorm:"primary_key"`
	DiscussionComment   DiscussionComment `json:"discussion_comment"`
	DiscussionCommentID int               `json:"discussion_comment_id" sql:"index"`
}

type DiscussionCommentTopicNotification struct {
	ID                         int                      `json:"id" gorm:"primary_key"`
	DiscussionCommentTopic     DiscussionCommentTopic   `json:"discussion_comment_topic"`
	DiscussionCommentTopicID   int                      `json:"discussion_comment_topic_id" sql:"index"`
	DiscussionCommentLike      DiscussionCommentLike    `json:"discussion_comment_like"`
	DiscussionCommentLikeID    int                      `json:"discussion_comment_like_id" sql:"index"`
	DiscussionCommentDisLike   DiscussionCommentDisLike `json:"discussion_comment_dislike"`
	DiscussionCommentDisLikeID int                      `json:"discussion_comment_dislike_id" sql:"index"`
	TemplateName               string                   `json:"template_name"`
	Opened                     bool                     `json:"opened" sql:"default:false"`
	CreatedAt                  time.Time
	UpdatedAt                  time.Time
}

type DiscussionCommentLike struct {
	LikeStruct
	DiscussionCommentID int `sql:"index"`
}

type DiscussionScore struct {
	DiscussionID int `json:"discussion_id"`
	Score        int `json"score"`
}

type DiscussionCommentDisLike struct {
	LikeStruct
	DiscussionCommentID int `sql:"index"`
}

type DiscussionCommentComplaint struct {
	LikeStruct
	DiscussionCommentID int `sql:"index"`
}
