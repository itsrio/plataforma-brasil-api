DROP table if exists discussion_comments_scores;
DROP view if exists discussion_comments_scores;
CREATE VIEW discussion_comments_scores AS (
select dc.discussion_id, (count(1) * 2) as score
from discussion_comments dc
where dc.created_at >= date_sub(curdate(), INTERVAL 1 WEEK) -- remove this for phase 3
group by dc.discussion_id);

drop table if exists discussion_likes_scores;
drop view if exists discussion_likes_scores;
CREATE VIEW discussion_likes_scores AS (
select dl.discussion_id, count(1) as score
from discussion_likes dl
where dl.created_at >= date_sub(curdate(), INTERVAL 1 WEEK)  -- remove this for phase 3
group by dl.discussion_id
);

drop table if exists discussion_dislikes_scores;
drop view if exists discussion_dislikes_scores;
CREATE VIEW discussion_dislikes_scores AS (
select dl.discussion_id, count(1) as score
from discussion_dislikes dl
where dl.created_at >= date_sub(curdate(), INTERVAL 1 WEEK)  -- remove this for phase 3
group by dl.discussion_id
);

drop table if exists discussion_sector_scores;
drop view if exists discussion_sector_scores;
CREATE VIEW discussion_sector_scores AS (
select d.id as discussion_id, (count(DISTINCT u.sector) * 10) as score
from users u
inner join discussion_comments dc on dc.user_id = u.id
inner join discussions d on d.id = dc.discussion_id
where
dc.created_at >= date_sub(curdate(), INTERVAL 1 WEEK) and  -- remove this for phase 3
u.sector <> ""
group by d.id
);


drop table if exists discussion_scores;
drop view if exists discussion_scores;
CREATE VIEW discussion_scores AS (
  select
    d.id as discussion_id,
    d.created_at as created_at,
    (
      COALESCE(dcs.score, 0) +
      COALESCE(dls.score, 0) +
      COALESCE(dds.score, 0) +
      COALESCE(dss.score, 0)) as score
  from discussions d
  left outer join discussion_comments_scores dcs on dcs.discussion_id = d.id
  left outer join discussion_likes_scores dls on dls.discussion_id = d.id
  left outer join discussion_dislikes_scores dds on dds.discussion_id = d.id
  left outer join discussion_sector_scores dss on dss.discussion_id = d.id
);
