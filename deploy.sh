#!/usr/bin/env bash

#
# simple commands executed at server to place new build
#
/sbin/service api-platbr stop
mv ~/go/bin/api-platbr ~/go/bin/api-platbr-old
mv ~/go/bin/api-platbr-new ~/go/bin/api-platbr
/sbin/service api-platbr start