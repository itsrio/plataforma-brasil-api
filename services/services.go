package services

import (
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"net/mail"
	"net/smtp"
	"os"

	"github.com/joho/godotenv"
)

func SendTextEmail(subj string, body string, from mail.Address, to mail.Address) error {
	return nil
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
		return err
	}

	from = mail.Address{"Plataforma Brasil", "nao-responda@plataformabrasil.org.br"}
	// Connect to the SMTP Server
	servername := os.Getenv("SMTP_SERVER")

	host, _, _ := net.SplitHostPort(servername)

	auth := smtp.PlainAuth("", os.Getenv("SMTP_USER"), os.Getenv("SMTP_PASS"), host)

	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         host,
	}

	// Setup headers
	headers := make(map[string]string)
	headers["MIME-version"] = "1.0"
	headers["Content-Type"] = "text/html; charset=\"UTF-8\""
	headers["From"] = from.String()
	headers["To"] = to.String()
	headers["Subject"] = subj

	// Setup message
	message := ""
	for k, v := range headers {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + body

	// Here is the key, you need to call tls.Dial instead of smtp.Dial
	// for smtp servers running on 465 that require an ssl connection
	// from the very beginning (no starttls)
	conn, err := tls.Dial("tcp", servername, tlsconfig)
	if err != nil {
		log.Fatal("Error %s", err)
		return err
	}

	c, err := smtp.NewClient(conn, host)
	if err != nil {
		log.Fatal("Error %s", err)
		return err
	}

	//Auth
	if err = c.Auth(auth); err != nil {
		log.Fatal("Error %s", err)
		return err
	}

	// To && From
	if err = c.Mail(from.Address); err != nil {
		log.Fatal("Error %s", err)
		return err
	}

	if err = c.Rcpt(to.Address); err != nil {
		log.Fatal("Error %s", err)
		return err
	}

	// Data
	w, err := c.Data()
	if err != nil {
		log.Fatal("Error %s", err)
		return err
	}

	_, err = w.Write([]byte(message))
	if err != nil {
		log.Fatal("Error %s", err)
		return err
	}

	err = w.Close()
	if err != nil {
		log.Fatal("Error %s", err)
		return err
	}

	c.Quit()
	return nil
}
