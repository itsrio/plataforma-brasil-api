package main

import (
	"time"

	"net/http"

	"github.com/codegangsta/negroni"
	"github.com/freehaha/token-auth"
	"github.com/freehaha/token-auth/jwt"
	"github.com/gorilla/mux"
	"github.com/meatballhat/negroni-logrus"
	"github.com/nucleo-digital/plataforma-brasil-api/controllers"
	"github.com/nucleo-digital/plataforma-brasil-api/middlewares"
	"github.com/thoas/stats"
	"github.com/unrolled/render"
	"github.com/zimmski/negroni-cors"
)

func main() {

	middleware := stats.New()
	n := negroni.Classic()
	r := mux.NewRouter().StrictSlash(true)

	n.Use(cors.NewAllow(&cors.Options{
		AllowAllOrigins: true,
		AllowMethods:    []string{"OPTIONS", "GET", "POST", "PUT", "DELETE"},
		AllowHeaders:    []string{"Content-Range", "Range-unit", "Origin", "Accept", "Content-Type", "Authorization"},
		ExposeHeaders:   []string{"Content-Range", "Range-unit"},
	}))

	n.Use(negronilogrus.NewMiddleware())

	n.Use(middleware)

	// db middleware
	n.Use(middlewares.NewDbConnection())

	// discussions handlers
	rDiscAuth := mux.NewRouter().StrictSlash(true)
	rDiscAuth.HandleFunc("/discussions", controllers.CreateDiscussionHandler).Methods("POST")
	rDiscAuth.HandleFunc("/discussions/{id:[0-9]+}/like", controllers.LikeDiscussionHandler).Methods("GET")
	rDiscAuth.HandleFunc("/discussions/{id:[0-9]+}/dislike", controllers.DisLikeDiscussionHandler).Methods("GET")
	rDiscAuth.HandleFunc("/discussions/{id:[0-9]+}/flag", controllers.CreateComplaintDiscussionHandler).Methods("POST")
	rDiscAuth.HandleFunc("/discussions/{id:[0-9]+}/comments", controllers.CreateCommentDiscussionHandler).Methods("POST")
	rDiscAuth.HandleFunc("/discussions/{id:[0-9]+}/comments/{comment_id:[0-9]+}/like", controllers.LikeCommentDiscussionHandler).Methods("GET")
	rDiscAuth.HandleFunc("/discussions/{id:[0-9]+}/comments/{comment_id:[0-9]+}/dislike", controllers.DisLikeCommentDiscussionHandler).Methods("GET")
	rDiscAuth.HandleFunc("/discussions/{id:[0-9]+}/comments/{comment_id:[0-9]+}/flag", controllers.CreateComplaintDiscussionCommentHandler).Methods("POST")
	// users handlers
	rUserAuth := mux.NewRouter().StrictSlash(true)
	rUserAuth.HandleFunc("/users", controllers.UserEditHandler).Methods("PUT")
	rUserAuth.HandleFunc("/users", controllers.UserHandler).Methods("GET")
	rUserAuth.HandleFunc("/users/configs", controllers.UserConfigHandler).Methods("GET")
	rUserAuth.HandleFunc("/users/configs", controllers.UserConfigCreateHandler).Methods("POST")
	rUserAuth.HandleFunc("/users/invite", controllers.UserInviteHandler).Methods("POST")
	rUserAuth.HandleFunc("/users/avatar", controllers.UserAvatarHandler).Methods("POST")
	rUserAuth.HandleFunc("/users/{id:[0-9]+}/discussions", controllers.DiscussionsFromUserHandler).Methods("GET")

	// priorities handlers
	rPriorityAuth := mux.NewRouter().StrictSlash(true)
	rPriorityAuth.HandleFunc("/priorities/vote", controllers.VoteHandler).Methods("POST")
	rPriorityAuth.HandleFunc("/priorities/skip", controllers.SkipHandler).Methods("POST")
	rPriorityAuth.HandleFunc("/priorities/choices", controllers.NewChoiceHandler).Methods("POST")
	rPriorityAuth.HandleFunc("/priorities/my_results", controllers.MyResultsHandler).Methods("GET")
	rPriorityAuth.HandleFunc("/priorities/results", controllers.AllResultsHandler).Methods("GET")
	rPriorityAuth.HandleFunc("/priorities/summary", controllers.MySummaryHandler).Methods("GET")
	rPriorityAuth.HandleFunc("/priorities/filter/{sector}/{region}", controllers.FilterAllResultsHandler).Methods("GET")

	admin_jwtst := jwtstore.New("admin_platbr", time.Hour*6)
	jwtst := jwtstore.New("platbr", time.Hour*6)

	rAdminAuth := mux.NewRouter().StrictSlash(true)
	rAdminAuth.HandleFunc("/admin/stats", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		data := middleware.Data()

		r := render.New()
		r.JSON(w, http.StatusOK, data)
	})
	rAdminAuth.HandleFunc("/admin/configs", controllers.SiteConfigHandler).Methods("GET")
	rAdminAuth.HandleFunc("/admin/configs", controllers.SiteConfigCreateHandler).Methods("POST")
	rAdminAuth.HandleFunc("/admin/users", controllers.AdminUserListHandler).Methods("GET")
	rAdminAuth.HandleFunc("/admin/users/count", controllers.AdminUserCountHandler).Methods("GET")
	rAdminAuth.HandleFunc("/admin/users/download", controllers.AdminUserListDownloadHandler).Methods("GET")
	rAdminAuth.HandleFunc("/admin/choices", controllers.AdminChoicesHandler).Methods("GET")
	rAdminAuth.HandleFunc("/admin/choices", controllers.AdminNewChoiceHandler).Methods("POST")
	rAdminAuth.HandleFunc("/admin/choices/{id}", controllers.AdminChoiceViewHandler).Methods("GET")
	rAdminAuth.HandleFunc("/admin/choices/{id}", controllers.AdminEditChoiceHandler).Methods("PUT")
	rAdminAuth.HandleFunc("/admin/phases", controllers.PublishedPhaseHandler).Methods("GET")
	rAdminAuth.HandleFunc("/admin/themes", controllers.AdminIndexThemesHandler).Methods("GET")
	rAdminAuth.HandleFunc("/admin/themes", controllers.CreateThemeHandler).Methods("POST")
	rAdminAuth.HandleFunc("/admin/phase", controllers.AdminPhasePublish).Methods("PUT")
	rAdminAuth.HandleFunc("/admin/discussions/{id:[0-9]+}/toggle_visibility", controllers.AdminDiscussionToggleVisibility).Methods("PUT")
	rAdminAuth.HandleFunc("/admin/discussion_comments/{id:[0-9]+}/toggle_visibility", controllers.AdminDiscussionCommentToggleVisibility).Methods("PUT")
	rAdminAuth.HandleFunc("/admin/comments", controllers.AdminDiscussionCommentsIndex).Methods("GET")
	r.PathPrefix("/admin").Handler(negroni.New(
		middlewares.NewTokenAuth(controllers.NotAuthorizedHandler, admin_jwtst, &tauth.BearerGetter{}),
		negroni.Wrap(rAdminAuth),
	))

	r.PathPrefix("/users").Handler(negroni.New(
		middlewares.NewTokenAuth(controllers.NotAuthorizedHandler, jwtst, &tauth.BearerGetter{}),
		negroni.Wrap(rUserAuth),
	))

	r.PathPrefix("/priorities").Handler(negroni.New(
		middlewares.NewTokenAuth(controllers.NotAuthorizedHandler, jwtst, &tauth.BearerGetter{}),
		negroni.Wrap(rPriorityAuth),
	))

	r.HandleFunc("/notifications/unreaded", controllers.TotalNotificationsUnread).Methods("GET")
	r.HandleFunc("/notifications/discussions", controllers.IndexDiscussionTopicNotificationsHandler).Methods("GET")
	r.HandleFunc("/notifications/discussions/{id:[0-9]+}", controllers.ShowDiscussionTopicNotificationsHandler).Methods("GET")
	r.HandleFunc("/notifications/discussion_comments", controllers.IndexDiscussionCommentTopicNotificationsHandler).Methods("GET")
	r.HandleFunc("/notifications/discussion_comments/{id:[0-9]+}", controllers.ShowDiscussionCommentTopicNotificationsHandler).Methods("GET")
	r.PathPrefix("/notifications").Handler(negroni.New(
		middlewares.NewTokenAuth(controllers.NotAuthorizedHandler, jwtst, &tauth.BearerGetter{}),
		negroni.Wrap(rDiscAuth),
	))

	r.HandleFunc("/discussions", controllers.IndexDiscussionHandler).Methods("GET")
	r.HandleFunc("/discussions/{id:[0-9]+}", controllers.ShowDiscussionHandler).Methods("GET")
	r.HandleFunc("/discussions/{id:[0-9]+}/comments", controllers.ShowCommentsDiscussionHandler).Methods("GET")
	r.PathPrefix("/discussions").Handler(negroni.New(
		middlewares.NewTokenAuth(controllers.NotAuthorizedHandler, jwtst, &tauth.BearerGetter{}),
		negroni.Wrap(rDiscAuth),
	))

	r.HandleFunc("/setupdb", controllers.MigrationHandler).Methods("GET")

	// generics handlers
	r.HandleFunc("/configs", controllers.SiteConfigHandler).Methods("GET")
	r.HandleFunc("/facebook", controllers.FacebookDetailsHandler).Methods("GET")
	r.HandleFunc("/aws/s3", controllers.AmazonS3TempUrlHandler).Methods("GET")
	r.HandleFunc("/phase", controllers.PublishedPhaseHandler).Methods("GET")
	r.HandleFunc("/themes", controllers.IndexThemesHandler).Methods("GET")
	r.HandleFunc("/themes/{id:[0-9]+}", controllers.ShowThemeHandler).Methods("GET")

	// users handlers
	r.HandleFunc("/login", controllers.UserLoginHandler).Methods("POST")
	r.HandleFunc("/reset_password/{email}", controllers.UserResetPasswordHandler).Methods("GET")
	r.HandleFunc("/change_password", controllers.UserChangePasswordHandler).Methods("POST")
	r.HandleFunc("/token_admin", controllers.AdminUserLoginHandler).Methods("POST")
	r.HandleFunc("/new_user", controllers.UserCreateHandler).Methods("POST")
	r.HandleFunc("/profile/{id:[0-9]+}", controllers.ProfileHandler).Methods("GET")

	// priorities handlers
	r.HandleFunc("/choices", controllers.ChoicesHandler).Methods("GET")

	r.HandleFunc("/list-choices", controllers.ListChoicesHandler).Methods("GET")
	// r.HandleFunc("/my_results", ResultsHandler).Methods("GET")

	// start listening
	n.UseHandler(r)
	n.Run(":3001")
}
